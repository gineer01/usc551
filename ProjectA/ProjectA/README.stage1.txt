Cuong Dong
CSCI 551
Project A

In this project, I've learned the socket API for Unix network programming.


a) I reused parts of the code from UNIX Network Programming Source Code, http://www.unpbook.com/src.html. They are functions declared in Unp.h and implemented in Unp.c. I didn't use all the codes provided on the website. I copied just the functions I needed into the Unp.h and Unp.c.



b) Yes, I completed this stage. Some details about my implementation:

- The client processes will connect to the server process and close the connection.

- The server process will iteratively accept the connections (it doesn't fork to concurrently accept and handle the connections). It will exit after accepting exactly N connections from child processes.

- The program prints out error and exits if the configuration file doesn't follow the specified syntax (e.g., if "num_nodes" or "nonce" is seen before "stage 1"). It also makes some assumptions about the syntax where it's not clearly specified (e.g., "nonce" is allowed before "num_nodes").

- I've tested with different values of "num_nodes". The program can handle up to 1000 nodes. I think this is because of the number of processes per user limit: the limit is 1024 on the provided VM. If the program is run with more than 1000 nodes, the user will have more than 1000 processes and, with other running processes, will reach the limit. If the number of nodes is too high and the limit is reached, the manager process will exit with "fork error" message and client processes will be unable to connect or read, resulting in "read error" or "connect error" messages.



c) The code will still work if the manager and client are on different CPU architectures. The "application protocol" uses decimal string representation to send nonce and "X PID" messsage. The conversion from decimal string representation to number should be done correctly regardless of CPU architecture. If the binary format had been used to send the integers, I would need to convert them to network byte-order before sending and convert back to host byte-order after receiving.


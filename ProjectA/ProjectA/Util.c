//
//  Util.c
//  ProjectA
//
//  Created by Cuong Dong on 9/27/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "Unp.h"


//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

void fileError(const char *message) {
    perror(message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Read a socket and ignore "Interrupted system call" until \n is found
int readSocket(int socket, char *buffer, int bufferSize){
	ssize_t n;
	int total = 0;
	buffer[total] = 0;
	while (strchr(buffer, '\n') == NULL){
		n = read(socket, buffer + total, bufferSize - total);
		if (n >= 0){		
			total += n;
			buffer[total] = 0;
		}
		else {
			if (errno == EINTR) {
				continue;
			}
			else {
				err_sys("read error");
			}
		}
	}
	return total;
}

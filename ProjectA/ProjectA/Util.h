//
//  Util.h
//  ProjectA
//
//  Created by Cuong Dong on 9/27/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectA_Util_h
#define ProjectA_Util_h

//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

//Read a socket and ignore "Interrupted system call" until \n is found
int readSocket(int socket, char *buffer, int bufferSize);

#endif

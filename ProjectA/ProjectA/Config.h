//
//  Config.h
//  ProjectA
//
//  Created by Cuong Dong on 9/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectA_Config_h
#define ProjectA_Config_h

typedef struct {
	int c; //number of client
	int nonce; // nonce for server
} Config;

Config readConfigFile(FILE *input);

#endif

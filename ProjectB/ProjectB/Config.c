//
//  Config.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "Util.h"
#include "Config.h"
#include "Common.h"


bool isComment(char *line){
	return line[0] == '#';
}

int getStage(char *line){
	int n = -1;
	sscanf(line, "stage %d", &n);
	return n;
}

Config readConfigFile(FILE *input){
	char line[BUFFER_SIZE_XL];
	Config config = {0, 0};
	
	bool hasStage = false;
	
	while (fgets(line, BUFFER_SIZE_XL, input) != NULL){
		if (isComment(line)){
			continue;
		}
		
		if (!hasStage){
			if ((config.stage = getStage(line)) > 0){//not comment and stage line
				hasStage = true;
				continue;
			}
			else {
				printError("Invalid line");
			}
		}
		
		int result = sscanf(line, "nonce %u", &config.nonce);
		if (result == 1) {
			return config;
		}
	}	

	printError("Unable to parse all 'stage x' and 'nonce'");
	return config;
}

bool getNextCommand(FILE *input, Command *command){
	char line[BUFFER_SIZE_XL];

	while (fgets(line, BUFFER_SIZE_XL, input) != NULL){
		if (isComment(line)){
			continue;
		}
		
		char token[30];
		sscanf(line, " %s %s", token, command->name);
		
		if (strcmp(token, START_COMMAND) == 0) {
			command->type = START;
		} else if (strcmp(token, TKCC_COMMAND) == 0) {
			command->type = TKCC;
		} else if (strcmp(token, STORE_COMMAND) == 0) {
			command->type = STORE;
		} else if (strcmp(token, SEARCH_COMMAND) == 0) {
			command->type = SEARCH;
		} else if (strcmp(token, END_COMMAND) == 0) {
			command->type = END;
		} else if (strcmp(token, KILL_COMMAND) == 0) {
			command->type = KILL;
		} else {
			fprintf(stderr, "Unexpected token %s\n", token);
			printError("Invalid command");
		}
		return true;
	}
	
	return false;
}
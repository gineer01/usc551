//
//  ChildProcess.h
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_ChildProcess_h
#define ProjectB_ChildProcess_h

#include <stdbool.h>
#include "Unp.h"

int runClientProcess(in_port_t port, int stage, bool isTkcc);

#endif

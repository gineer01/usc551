//
//  Triad.h
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Triad_h
#define ProjectB_Triad_h


#include "TriadMessage.h"
#include "App.h"

void respond(App *app, Node *thisNode, Message *query, int port);

void checkTimeout(Node *node);
#endif

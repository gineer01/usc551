//
//  CommandParser.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "CommandParser.h"

//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "The commandline syntax for projectA is as follows:\n"
    "projectA <input_file_path>\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, const char *argv[]){
	CommandOptions options = {""};
	
    if (argc != 2) {
        printErrorAndUsage("Please provide the path for the input file\n");
    }
	
	options.path = argv[1];
	
	return options;
}

//
//  Node.c
//  ProjectB
//
//  Created by Cuong Dong on 10/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "Unp.h"
#include "Util.h"
#include "Node.h"
#include "sha1.h"


NodeInfo closestPrecedingFinger(Node *thisNode, HashId id){
	int m = getMaxFinger(thisNode->stage);
	for (int i = m; i >= 1; i--){
//		printf("Node %08x finger %d is %08x\n", thisNode->node.hash, i, thisNode->fingers[i].hash);
		if (isInRangeOpen(thisNode->fingers[i].hash, thisNode->node.hash, id)){
			return thisNode->fingers[i];
		}
	}
	return thisNode->node;
}

HashId triadHash(uint32_t nonce, char *name)
{
	size_t name_length = strlen(name);
	size_t nonce_length = sizeof(uint32_t);
	size_t buffer_length =  nonce_length + name_length;
	unsigned char *buffer = malloc(buffer_length);
	
	uint32_t networkByteOrder = htonl(nonce);
	
	memcpy(buffer, &networkByteOrder, nonce_length);
	memcpy(buffer + nonce_length, name, name_length);
	
	unsigned int result = projb_hash(buffer, (int) buffer_length);
	free(buffer);
	
	return result;
}

void updateFinger(Node *thisNode, int i, NodeInfo info){
	thisNode->fingers[i] = info;
}

bool containsData(Node *thisNode, HashId data){
	for (int i = 0; i < MAX_DATA; i++) {
		if (thisNode->data[i] == data){
			return true;
		}
	}
	return false;
}

int findNextData(Node *thisNode, int queryId){
	int index = -1;
	int maxId = 0;
	for (int i = 0; i < MAX_DATA; i++) {
		if (thisNode->dataS[i] == NULL) continue;
		
		HashId temp = thisNode->data[i];
		if (temp > queryId) continue;
		if (temp > maxId){
			maxId = temp;
			index = i;
		}
		
	}
	return index;
}

void initNode(Node *thisNode, int stage, bool isTkcc, uint32_t nonce, char *name, char *fs, int fp, int udpPort, Kernel *k){
	//compute hash
	HashId thisNodeId = triadHash(nonce, name);
	HashId firstNodeId = triadHash(nonce, fs);
	
	Node temp = {
		{thisNodeId, udpPort},
		{firstNodeId, fp},

		stage,
		nonce,
		isTkcc,
		k
	};
	*thisNode = temp;
	for (int i = 0; i < MAX_DATA; i++) {
		thisNode->data[i] = 0;
		thisNode->dataS[i] = NULL;
	}
}

int saveDataWithoutValidation(Node *thisNode, char *s, HashId dataId) {
	if (thisNode->isTkcc){
		s = "nyah, nyah";
	}
    for (int i = 0; i < MAX_DATA; i++) {
		if (thisNode->data[i] == 0){
			thisNode->data[i] = dataId;
			size_t l = strlen(s) + 1;
			thisNode->dataS[i] = malloc(l);
			strcpy(thisNode->dataS[i], s);
			return STORE_SUCCESS;
		}
	}
	return STORE_FAIL;//cannot add data 
}

int storeData(Node *thisNode, HashId dataId, char *s){
	if (!isInRangeLeftOpen(dataId, thisNode->fingers[PREDECESSOR_INDEX].hash, thisNode->node.hash)){
		if (thisNode->stage != 7){
			fprintf(stderr, "Data with hash %08x doesn't belong to node %08x\n", dataId, thisNode->node.hash);
			return STORE_FAIL; //data does not belong to thisNode
		}
	}
	if (containsData(thisNode, dataId)){
		return STORE_FOUND;
	}
	return saveDataWithoutValidation(thisNode, s, dataId);
}






//
//  TriadMessage.h
//  ProjectB
//
//  Created by Cuong Dong on 10/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_TriadMessage_h
#define ProjectB_TriadMessage_h

#include <stdbool.h>
#include "Common.h"

typedef struct MessageStruct {
	MessageType type;
	HashId receiverId;//ni
	
	HashId nodeId; //si/pi/ri
	int port; //sp/pp/rp
	
	HashId queryId; //di
	unsigned int successCode;//has/R
	unsigned int i;
	
	unsigned int length;
	char s[MESSAGE_LENGTH];
} Message;

size_t encodeMessage(Message *m, char *buffer);
void decodeMessage(Message *m, char *buffer, size_t size);
void logMessage(FILE *log, Message *m, bool isSend);

#endif

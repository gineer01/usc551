//
//  main.c
//  ProjectA
//
//  Created by Cuong Dong on 9/27/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "CommandParser.h"
#include "Util.h"
#include "Config.h"
#include "ChildProcess.h"
#include "Unp.h"


void runManagerProcess(int listeningSocket, Config config, FILE *log){
	char nonceString[64];
	snprintf(nonceString, sizeof(nonceString), "%d\n", config.nonce);
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	socklen_t len = sizeof(address);
	
	for (int count = 1; count <= config.c; count++){
		int connectedSocket = Accept(listeningSocket, (SA *) &address, &len);
		fprintf(log, "client %d port: %d\n", count, ntohs(address.sin_port));
		printf("client %d port: %d\n", count, ntohs(address.sin_port));
		Write(connectedSocket, nonceString, strlen(nonceString));
		
		const int BUFFER_SIZE = 256;
		char buffer[BUFFER_SIZE];
		int total = readSocket(connectedSocket, buffer, BUFFER_SIZE);		
		buffer[total] = 0;
		printf("client %d says: %s", count, buffer);
		fprintf(log, "client %d says: %s", count, buffer);
//		Close(connectedSocket);
	}
}

int main(int argc, const char * argv[])
{
	CommandOptions options = parseCommandLine(argc, argv);	
	FILE *input = openFile(options.path);
	Config config = readConfigFile(input);
	fclose(input);	
	printf("Clients: %d\n", config.c);
	printf("Nonce: %d\n", config.nonce);	
	
	//open a listening socket
	int listeningSocket = Socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);	
	Bind(listeningSocket, (SA *) &address, sizeof(address));	
	Listen(listeningSocket, config.c);	
	
	//get port number
	socklen_t len = sizeof(address);
	memset(&address, 0, sizeof(address));
	Getsockname(listeningSocket, (SA *) &address, &len);
	in_port_t port = ntohs(address.sin_port);
	
	//flush before forking; otherwise, children get the same
	//string in buffer and print the same thing multiple times
	fflush(stdout);
	
	for (int i = 0; i < config.c; i++) {
		pid_t pid = Fork();
		if (pid == 0) {
			Close(listeningSocket);
			_exit(runClientProcess(port));
		}
	}
	
	FILE *log = openOutputFile("stage1.manager.out");
	fprintf(log, "manager port: %d\n", port);
	printf("manager port: %d\n", port);
	runManagerProcess(listeningSocket, config, log);
	Close(listeningSocket);
	fclose(log);
    return 0;
}


//
//  Kernel.c
//  ProjectB
//
//  Created by Cuong Dong on 10/31/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include "Util.h"
#include "Kernel.h"

FILE * openLogFile(char* name, int stage) {
    char fileName[MAX_FILE_NAME];
	snprintf(fileName, MAX_FILE_NAME, "stage%d.%s.out", stage, name);
    return Fopen(fileName, "w");
}

void initKernel(Kernel *k, char *name, int stage, int udpSocket){
	k->log = openLogFile(name, stage);
	k->isRaw = (strcmp("logmsg", name) == 0);
	k->udpSocket = udpSocket;
	k->nextOutbound = -1;
	for (int i = 0; i < MAX_CLIENT; i++){
		k->contexts[i] = NULL;
		k->outbound[i] = NULL;
		k->inbound[i] = NULL;
	}
}

void receiveMessage(Kernel *thisNode, Message *m, int toSocket, SA *fromAddress, socklen_t *fromAddressLength){
	char buffer[BUFFER_SIZE];
	size_t size = Recvfrom(toSocket, buffer, BUFFER_SIZE, 0, fromAddress, fromAddressLength);
	
	decodeMessage(m, buffer, size);
	
	logMessage(thisNode->log, m, false);
	if (thisNode->isRaw){
		logRawMessage(thisNode->log, buffer, size);
	}
}

in_port_t receiveMessageSimple(Kernel *thisNode, Message *m, int toSocket){
	struct sockaddr_in address;
	socklen_t len = sizeof(address);
	
	receiveMessage(thisNode, m, toSocket, (SA*)&address, &len);
	return ntohs(address.sin_port);
}



void sendMessage(Kernel *thisNode, Message *m, int fromSocket, int toPort){
	struct sockaddr_in address;
	socklen_t len = sizeof(address);
	memset(&address, 0, len);
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(0x7F000001);//127.0.0.1
	address.sin_port = htons(toPort);
	
	char buffer[BUFFER_SIZE];
	size_t size = encodeMessage(m, buffer);
	
	Sendto(fromSocket, buffer, size, 0, (SA *)&address, len);
	
	logMessage(thisNode->log, m, true);
}

//Returns false if queue is full
bool addToQueue(QueueItem *q[MAX_CLIENT], QueueItem *item){
	for (int i = 0; i < MAX_CLIENT; i++){
		if (q[i] == NULL){
			q[i] = item;
			return true;
		}
	}
	return false;
}


int checkQueue(QueueItem *q[MAX_CLIENT]){
	for (int i = 0; i < MAX_CLIENT; i++){
		if (q[i] != NULL){
			return i;
		}
	}
	return -1;
}

int checkContext(Kernel *thisNode, Message *m){
	for (int i = 0; i < MAX_CLIENT; i++){
		if (thisNode->contexts[i] != NULL){
			ExecContext *ec = thisNode->contexts[i];
//			printf("Context [%d] type %d\n", i, m->type);
			if ((ec->sender == m->receiverId) && (ec->type == m->type)){
				switch (m->type) {
					case STORES_R:
						if (ec->local.queryId == m->queryId){
							return i;
						}
						break;
						
					default:
						return i;
				}
			}
			//Special handling to ignore update query
			if ((m->type == UPDATE_LEAVING_Q) && (ec->type == m->type)){
				return i;
			}
		}
	}
	return -1;
}

bool addToContext(Kernel *k, ExecContext *item){
	for (int i = 0; i < MAX_CLIENT; i++){
		if (k->contexts[i] == NULL){
			k->contexts[i] = item;
			return true;
		}
	}
	return false;
}

void freeQueueItem(QueueItem *i) {
    free(i->m);
    free(i);
}

bool queueInbound(int port, Message *m, Kernel *thisNode) {
    QueueItem *i = malloc(sizeof(QueueItem));
    i->port = port;
    i->m = m;
    bool isAdded = addToQueue(thisNode->inbound, i);
    if (!isAdded){
        freeQueueItem(i);
    }
	return isAdded;
}

bool queueOutbound(int port, Message *m, Kernel *thisNode) {
    QueueItem *i = malloc(sizeof(QueueItem));
    i->port = port;
    i->m = m;
    bool isAdded = addToQueue(thisNode->outbound, i);
    if (!isAdded){
        freeQueueItem(i);
    }
	return isAdded;
}

void queueMessageAndContext(Kernel *thisNode, Message *newQuery, int port, ExecContext *context) {
  	bool isAdded = queueOutbound(port, newQuery, thisNode);
	if (isAdded){
		if (context == NULL) return;
		
		bool isContextAdded = addToContext(thisNode, context);
		if (!isContextAdded){
			free(context);
			fprintf(stderr, "SEVERE: Cannot queue context. Increase context queue size?\n");
		}
	}
	else {
		free(newQuery);
		if (context != NULL) free(context);
		fprintf(stderr, "SEVERE: Cannot queue outbound message. Increase outbound queue size?\n");
	}
}



void sendMessageQueue(Kernel *thisNode){
//	printf("Sending message at %d\n", thisNode->nextOutbound);
	if (thisNode->nextOutbound < 0) return;
	
	int i = thisNode->nextOutbound;
	if (thisNode->outbound[i] != NULL){
		QueueItem *item = thisNode->outbound[i];
//		printf("Send a Triad message %d to port %d\n", i, item->port);
		sendMessage(thisNode, item->m, thisNode->udpSocket, item->port);
		freeQueueItem(item);
	}
	thisNode->outbound[i] = NULL;
	thisNode->nextOutbound = checkQueue(thisNode->outbound);
//	printf("Next outbound is %d\n", thisNode->nextOutbound);
}

int checkMessageToSend(Kernel *k){
	if (k->nextOutbound < 0){
		k->nextOutbound = checkQueue(k->outbound);
	}
	
	return k->nextOutbound;
}

void initContext(ExecContext *ec, HashId sender, MessageType type, Algorithm algo, int step, char *name){
	ec->sender = sender;
	ec->type = type;
	ec->algo = algo;
	ec->step = step;
	
	if (name != NULL){
		size_t size = strlen(name) + 1;
		ec->local.name = malloc(size);
		memcpy(ec->local.name, name, size);
	}
	else {
		ec->local.name = NULL;
	}
	
	gettimeofday(&ec->expireAt, NULL);
	ec->expireAt.tv_sec += CLIENT_WAIT;
}

void freeContext(Kernel *thisNode, int i){
	char *name = thisNode->contexts[i]->local.name;
	if (name != NULL){
		free(name);
	}
	free(thisNode->contexts[i]);
	thisNode->contexts[i] = NULL;
}
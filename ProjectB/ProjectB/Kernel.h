//
//  Kernel.h
//  ProjectB
//
//  Created by Cuong Dong on 10/31/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Kernel_h
#define ProjectB_Kernel_h

#include "Common.h"
#include "TriadMessage.h"
#include "Unp.h"

typedef enum {
	FIND_SUCCESSOR_2,
	APP_STORE,
	APP_SEARCH,
	GET_ALL_DATA,
	HELLO_PREDECESSOR,
	DO_NOTHING
} Algorithm;

typedef struct {
	int returnPort;
	void *app;
	char *name;
	
	HashId queryId;//in case there are multiple stores-q queries
	
	//stage 7
	HashId storeId;
	int searchAttempt;
} LocalVal;

typedef struct {
	HashId sender;
	MessageType type;
	
	Algorithm algo;
	int step;
	struct timeval expireAt;
	
	LocalVal local;
} ExecContext;

typedef struct {
	Message *m;
	int port;
} QueueItem;

typedef struct {
	bool isRaw;
	int udpSocket;
	int nextOutbound;
	FILE *log;
	
	ExecContext *contexts[MAX_CLIENT];
	QueueItem *outbound[MAX_CLIENT];
	QueueItem *inbound[MAX_CLIENT];
} Kernel;

void initKernel(Kernel *k, char *name, int stage, int udpSocket);

void sendMessage(Kernel *thisNode, Message *m, int fromSocket, int toPort);

void receiveMessage(Kernel *thisNode, Message *m, int toSocket, SA *fromAddress, socklen_t *fromAddressLength);

//Returns the port number of the sender to reply back
in_port_t receiveMessageSimple(Kernel *thisNode, Message *m, int toSocket);



bool queueOutbound(int port, Message *m, Kernel *thisNode);
bool queueInbound(int port, Message *m, Kernel *thisNode);
bool addToContext(Kernel *k, ExecContext *item);
int checkQueue(QueueItem *q[MAX_CLIENT]);
int checkContext(Kernel *thisNode, Message *m);
void sendMessageQueue(Kernel *thisNode);
int checkMessageToSend(Kernel *thisNode);

void queueMessageAndContext(Kernel *thisNode, Message *newQuery, int port, ExecContext *context);
void initContext(ExecContext *ec, HashId sender, MessageType type, Algorithm algo, int step, char *name);
void freeContext(Kernel *thisNode, int i);


#endif

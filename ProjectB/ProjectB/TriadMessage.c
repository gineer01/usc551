//
//  TriadMessage.c
//  ProjectB
//
//  Created by Cuong Dong on 10/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include "TriadMessage.h"
#include "Util.h"
#include "Unp.h"


size_t encodeInteger(unsigned int i, char *buffer){
	uint32_t networkByteOrder = htonl(i);
	size_t length = sizeof(uint32_t);
	memcpy(buffer, &networkByteOrder, length);
	return length;
}

size_t encodeString(char *s, int length, char *buffer){
	memcpy(buffer, s, length);
	return length;
}


size_t encodeMessage(Message *m, char *buffer){
	size_t length = 0;
	length += encodeInteger(m->type, buffer + length);
	length += encodeInteger(m->receiverId, buffer + length);
	switch (m->type) {
		case SUCCESSOR_Q:
		case PREDECESSOR_Q:
		case HELLO_PREDECESSOR_Q:
		case LEAVING_R:
			break;
			
		case SUCCESSOR_R:
		case PREDECESSOR_R:
		case HELLO_PREDECESSOR_R:
			length += encodeInteger(m->nodeId, buffer + length);
			length += encodeInteger(m->port, buffer + length);
			break;
			
		case LEAVING_Q:			
		case NEXT_DATA_Q:
		case STORES_Q:
			length += encodeInteger(m->queryId, buffer + length);
			break;
			
		case STORES_R:
			length += encodeInteger(m->queryId, buffer + length);
			length += encodeInteger(m->nodeId, buffer + length);
			length += encodeInteger(m->port, buffer + length);
			length += encodeInteger(m->successCode, buffer + length);
			length += encodeInteger(m->length, buffer + length);
			length += encodeString(m->s, m->length, buffer + length);
			break;
			
		case UPDATE_Q:
			length += encodeInteger(m->nodeId, buffer + length);
			length += encodeInteger(m->port, buffer + length);
			length += encodeInteger(m->i, buffer + length);
			break;
			
		case UPDATE_LEAVING_Q:
		case UPDATE_LEAVING_R:
			length += encodeInteger(m->nodeId, buffer + length);
			length += encodeInteger(m->port, buffer + length);
			length += encodeInteger(m->queryId, buffer + length);
			break;
			
		case UPDATE_R:
			length += encodeInteger(m->successCode, buffer + length);
			length += encodeInteger(m->nodeId, buffer + length);
			length += encodeInteger(m->port, buffer + length);
			length += encodeInteger(m->i, buffer + length);
			break;
			
		case STORE_PLEASE_Q:
			length += encodeInteger(m->queryId, buffer + length);
			length += encodeInteger(m->length, buffer + length);
			length += encodeString(m->s, m->length, buffer + length);
			break;
			
		case STORE_PLEASE_R:
			length += encodeInteger(m->successCode, buffer + length);
			length += encodeInteger(m->length, buffer + length);
			length += encodeString(m->s, m->length, buffer + length);
			break;
			
		case NEXT_DATA_R:
			length += encodeInteger(m->queryId, buffer + length);//qid
			length += encodeInteger(m->nodeId, buffer + length);//rid
			length += encodeInteger(m->length, buffer + length);
			length += encodeString(m->s, m->length, buffer + length);
			break;
			
		default:
			fprintf(stderr, "encodeMessage: Unexpected message type. %d\n", m->type);
			break;
	}
//	printf("Encoded message length: %zd. ", length);
//	logRawMessage(stdout, buffer, length);
	return length;
}



typedef enum {
	MESSAGE_TYPE,
	RECEIVER_ID,
	NODE_ID,
	PORT,
	QUERY_ID,
	SUCCESS_CODE,
	FINGER_I,
	LENGTH
} Field;

size_t decodeInteger(Message *m, Field f, char *buffer){
	uint32_t networkByteOrder;
	size_t length = sizeof(uint32_t);
	memcpy(&networkByteOrder, buffer, length);
	
	unsigned int value = ntohl(networkByteOrder);
	
	switch (f) {
		case MESSAGE_TYPE:
			m->type = value;
			break;
		case RECEIVER_ID:
			m->receiverId = value;
			break;
		case NODE_ID:
			m->nodeId = value;
			break;
		case PORT:
			m->port = value;
			break;
		case QUERY_ID:
			m->queryId = value;
			break;
		case SUCCESS_CODE:
			m->successCode = value;
			break;
		case FINGER_I:
			m->i = value;
			break;
		case LENGTH:
			m->length = value;
			break;
		default:
			printError("Unexpected field type in decodeInteger");
			break;
	}
	
	return length;
}

size_t decodeString(Message *m, int length, char *buffer){
	memcpy(m->s, buffer, length);
	m->s[length] = 0;//null-terminate string
	return length;
}

void decodeMessage(Message *m, char *buffer, size_t size){
	size_t length = 0;
	length += decodeInteger(m, MESSAGE_TYPE, buffer + length);
	length += decodeInteger(m, RECEIVER_ID, buffer + length);
	
	switch (m->type) {
		case SUCCESSOR_Q:
		case PREDECESSOR_Q:
		case HELLO_PREDECESSOR_Q:
		case LEAVING_R:
			break;
		case SUCCESSOR_R:
		case PREDECESSOR_R:
		case HELLO_PREDECESSOR_R:
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, PORT, buffer + length);
			break;
			
		case STORES_Q:
		case LEAVING_Q:
		case NEXT_DATA_Q:
			length += decodeInteger(m, QUERY_ID, buffer + length);
			break;
			
		case STORES_R:
			length += decodeInteger(m, QUERY_ID, buffer + length);
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, PORT, buffer + length);
			length += decodeInteger(m, SUCCESS_CODE, buffer + length);
			length += decodeInteger(m, LENGTH, buffer + length);
			length += decodeString(m, m->length, buffer + length);
			break;
			
		case UPDATE_Q:
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, PORT, buffer + length);
			length += decodeInteger(m, FINGER_I, buffer + length);
			break;
			
		case UPDATE_LEAVING_R:
		case UPDATE_LEAVING_Q:
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, PORT, buffer + length);
			length += decodeInteger(m, QUERY_ID, buffer + length);
			break;
			
		case UPDATE_R:
			length += decodeInteger(m, SUCCESS_CODE, buffer + length);
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, PORT, buffer + length);
			length += decodeInteger(m, FINGER_I, buffer + length);
			break;
			
		case STORE_PLEASE_Q:
			length += decodeInteger(m, QUERY_ID, buffer + length);
			length += decodeInteger(m, LENGTH, buffer + length);
			length += decodeString(m, m->length, buffer + length);
			break;
			
		case STORE_PLEASE_R:
			length += decodeInteger(m, SUCCESS_CODE, buffer + length);
			length += decodeInteger(m, LENGTH, buffer + length);
			length += decodeString(m, m->length, buffer + length);
			break;
			
		case NEXT_DATA_R:
			length += decodeInteger(m, QUERY_ID, buffer + length);
			length += decodeInteger(m, NODE_ID, buffer + length);
			length += decodeInteger(m, LENGTH, buffer + length);
			length += decodeString(m, m->length, buffer + length);
			break;
			
		default:
			fprintf(stderr, "decodeMessage: Unexpected message type. %d\n", m->type);
			break;
	}
	
//	printf("Decoded message length: %zd. ", size);
//	logRawMessage(stdout, buffer, size);
	
	if (length != size){
		fprintf(stderr, "WARNING: Decoding message returns a different size from buffer: %zd vs. %zd\n", length, size);
	}
}


char* getMessageName(MessageType type){
	switch (type) {
		case SUCCESSOR_Q:
			return "successor-q";
		case PREDECESSOR_Q:
			return "predecessor-q";
		case HELLO_PREDECESSOR_Q:
			return "hello-predecessor-q";
		case SUCCESSOR_R:
			return "successor-r";
		case PREDECESSOR_R:
			return "predecessor-r";
		case HELLO_PREDECESSOR_R:
			return "hello-predecessor-r";
		case STORES_Q:
			return "ext-stores-q";
		case STORES_R:
			return "ext-stores-r";
		case UPDATE_Q:
			return "update-q";
		case UPDATE_R:
			return "update-r";
		case STORE_PLEASE_Q:
			return "store-q";
		case STORE_PLEASE_R:
			return "store-r";
		case LEAVING_Q:
			return "leaving-q";
		case UPDATE_LEAVING_Q:
			return "update-leaving-q";
		case UPDATE_LEAVING_R:
			return "update-leaving-r";
		case LEAVING_R:
			return "leaving-r";
		case NEXT_DATA_Q:
			return "next-data-q";
		case NEXT_DATA_R:
			return "next-data-r";

		default:
			fprintf(stderr, "getMessageName: Unexpected message type. %d\n", type);
			break;
	}
	return "error";
}


void printHashId(HashId id, FILE *log){
	fprintf(log, "0x%08x", id);
}

void printInteger(unsigned int i, FILE *log){
	fprintf(log, "%d", i);
}

void printString(char *s, FILE *log){
	fprintf(log, "'%s'", s);
}

#define SPACE fprintf(log, " ");

void logMessage(FILE *log, Message *m, bool isSend){
	fprintf(log, "%s %s (", getMessageName(m->type), isSend ? "sent" : "received");
	printHashId(m->receiverId, log);
	switch (m->type) {
		case SUCCESSOR_Q:
		case PREDECESSOR_Q:
		case HELLO_PREDECESSOR_Q:
		case LEAVING_R:
			break;
			
		case SUCCESSOR_R:
		case PREDECESSOR_R:
		case HELLO_PREDECESSOR_R:
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->port, log);
			break;
			
		case LEAVING_Q:
		case NEXT_DATA_Q:
		case STORES_Q:
			SPACE
			printHashId(m->queryId, log);
			break;
			
		case STORES_R:
			SPACE
			printHashId(m->queryId, log);
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->port, log);
			SPACE
			printInteger(m->successCode, log);
			SPACE
			printInteger(m->length, log);
			SPACE
			printString(m->s, log);
			break;
			
		case UPDATE_Q:
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->port, log);
			SPACE
			printInteger(m->i, log);
			break;
			
		case UPDATE_LEAVING_Q:
		case UPDATE_LEAVING_R:
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->port, log);
			SPACE
			printHashId(m->queryId, log);
			break;
			
		case UPDATE_R:
			SPACE
			printInteger(m->successCode, log);
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->port, log);
			SPACE
			printInteger(m->i, log);
			break;
			
		case STORE_PLEASE_Q:
			SPACE
			printHashId(m->queryId, log);
			SPACE
			printInteger(m->length, log);
			SPACE
			printString(m->s, log);
			break;
			
		case STORE_PLEASE_R:
			SPACE
			printInteger(m->successCode, log);
			SPACE
			printInteger(m->length, log);
			SPACE
			printString(m->s, log);
			break;
			
		case NEXT_DATA_R:
			SPACE
			printHashId(m->queryId, log);
			SPACE
			printHashId(m->nodeId, log);
			SPACE
			printInteger(m->length, log);
			SPACE
			printString(m->s, log);
			break;
			
		default:
			fprintf(stderr, "logMessage: Unexpected message type. %d\n", m->type);
			break;
	}
	fprintf(log, ")\n");
}




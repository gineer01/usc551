//
//  ChildProcess.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "ChildProcess.h"
#include "Util.h"
#include "TriadProtocol.h"
#include "App.h"
#include "Algorithm.h"

void readSetupMessage(int tcpSocket, uint32_t *nonce, char* name, uint16_t *fp, char* fs) {
	char buffer[SETUP_MESSAGE];
	
	int total = readSocket(tcpSocket, buffer, SETUP_MESSAGE, 4);
	buffer[total] = 0;
//	printf("Setup message received: %s", buffer);
	sscanf(buffer, "%u %s %hu %s", nonce, name, fp, fs);
}

void connectTcp(in_port_t port, int tcpSocket) {
    struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(0x7F000001);//127.0.0.1
	address.sin_port = htons(port);
	socklen_t len = sizeof(address);
	
	Connect(tcpSocket, (SA *) &address, len);
}


void handleTriadMessage(App *app, Node *thisNode, int udpSocket){
	Message query;
	int port = receiveMessageSimple(thisNode->k, &query, udpSocket);

	respond(app, thisNode, &query, port);
}

void writeTriadMessage(Node *thisNode) {
	sendMessageQueue(thisNode->k);
}



void setFdSet(bool condition, fd_set *fdSet, int socket) {
    if (condition){
        FD_SET(socket, fdSet);
    }
    else {
        FD_CLR(socket, fdSet);
    }
}

void listenForNewMessage(Node *thisNode, App *app){
	int tcpSocket = app->tcpSocket;
	int udpSocket = thisNode->k->udpSocket;
	printf("Node %08x listening UDP %d\n", thisNode->node.hash, thisNode->node.port);

	fd_set readingSet, writingSet;
	int maxFd = tcpSocket > udpSocket ? tcpSocket : udpSocket;
	FD_ZERO(&readingSet);
	FD_ZERO(&writingSet);
	FD_SET(tcpSocket, &readingSet);
	FD_SET(udpSocket, &readingSet);
	
	struct timeval duration = {CLIENT_INTERVAL, 0};
	struct timeval *selectDuration = thisNode->stage == 6 ? &duration : NULL;
	
	while (true) {
		if (app->timeToDie){
			sendOk(app);
			printf("Terminating %08x\n", thisNode->node.hash);
			break;
		}
		
		int hasMessage = checkMessageToSend(thisNode->k);
		setFdSet(hasMessage >= 0, &writingSet, udpSocket);		
		setFdSet(app->isSendOk, &writingSet, tcpSocket);
		setFdSet(!app->isBusy, &readingSet, tcpSocket);
				
		fd_set tempRead = readingSet;
		fd_set tempWrite = writingSet;
		
		int nReady;
		do {
			nReady = select(maxFd+1, &tempRead, &tempWrite, NULL, selectDuration);
		} while ((nReady < 0) && (errno == EINTR));		
//		printf("End listening %d %d %d\n", nReady, errno, EINTR);
		
		if ((nReady == 0) && (thisNode->stage == 6)){
//			printf("Timeout. Check expiration.\n");
			checkTimeout(thisNode);
			continue;
		}
		
		if (FD_ISSET(tcpSocket, &tempRead)){
//			printf("Receive TCP message\n");
			handleTcpCommand(app);
			
			if (--nReady <= 0) continue;
		}
		
		if (FD_ISSET(tcpSocket, &tempWrite)){
//			printf("Ready to send TCP message\n");
			sendOk(app);
			app->isBusy = false;
			
			if (--nReady <= 0) continue;
		}
		
		if (FD_ISSET(udpSocket, &tempRead)){
//			printf("Receive Triad message\n");
			handleTriadMessage(app, thisNode, udpSocket);
						
			if (--nReady <= 0) continue;
		}
		
		if (FD_ISSET(udpSocket, &tempWrite)){
//			printf("Ready to send Triad message\n");
			writeTriadMessage(thisNode);
			
			if (--nReady <= 0) continue;
		}
	}
}

in_port_t openUdpPort(int udpSocket) {
    struct sockaddr_in address;
	socklen_t len = sizeof(address);
	memset(&address, 0, len);
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	Bind(udpSocket, (SA *) &address, len);
	
	//get port number
	memset(&address, 0, len);
	Getsockname(udpSocket, (SA *) &address, &len);
	in_port_t udpPort = ntohs(address.sin_port);
    return udpPort;
}


int runClientProcess(in_port_t port, int stage, bool isTkcc){
	//	printf("Server Port: %d\n", port);
	int tcpSocket = Socket(AF_INET, SOCK_STREAM, 0);
	connectTcp(port, tcpSocket);
	
	//open udp port
	int udpSocket = Socket(AF_INET, SOCK_DGRAM, 0);
	in_port_t udpPort = openUdpPort(udpSocket);
	
	char name[MAX_NAME];
	uint32_t nonce;
	char fs[MAX_NAME];
	uint16_t fp;
    readSetupMessage(tcpSocket, &nonce, name, &fp, fs);
	
	Kernel kernel;
	initKernel(&kernel, name, stage, udpSocket);
	Node thisNode;
	bool tkccMode = (stage == 7) && isTkcc;
	if (tkccMode) {
		printf("Node %s is in TKCC mode\n", name);
	}
	initNode(&thisNode, stage, tkccMode, nonce, name, fs, fp, udpPort, &kernel);
	App app;
	initApp(&app, tcpSocket, &thisNode);
	
	//ask to enter Triad ring
	triadJoin(&thisNode);

	fprintf(stdout, "client %s created with hash 0x%08x with port %d\n", name, thisNode.node.hash, udpPort);
	printf("Prev: %08x . Next: %08x\n", thisNode.fingers[0].hash, thisNode.fingers[1].hash);
	fprintf(kernel.log, "client %s created with hash 0x%08x\n", name, thisNode.node.hash);
	printf("**** Node %s (hash %08x) %s has joined Triad ring ****\n\n", name, thisNode.node.hash, thisNode.isTkcc ? "(TKCC)" : "");
	
	sendReadyToManager(&app);

	listenForNewMessage(&thisNode, &app);
	
	Fclose(kernel.log);
	Close(tcpSocket);
	Close(udpSocket);
	return 0;
}
//
//  CommandParser.h
//  ProjectA
//
//  Created by Cuong Dong on 9/27/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectA_CommandParser_h
#define ProjectA_CommandParser_h

//struct to capture all possible command options
typedef struct {
    const char *path;
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, const char *argv[]);

#endif

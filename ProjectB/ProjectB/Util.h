//
//  Util.h
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Util_h
#define ProjectB_Util_h

#include <stdbool.h>
#include "Common.h"


//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

//Read a socket and ignore "Interrupted system call" until numOfLines are found
int readSocket(int socket, char *buffer, int bufferSize, int numOfLines);

void logRawMessage(FILE *log, char *buffer, size_t size);

bool isInRangeLeftOpen(HashId queryId, HashId startExclusive, HashId endInclusive);
bool isInRangeOpen(HashId queryId, HashId startExclusive, HashId endExclusive);
bool isInRangeRightOpen(HashId queryId, HashId startInclusive, HashId endExclusive);

HashId getFingerStart(HashId nodeId, int k);
int getMaxFinger(int stage);
#endif

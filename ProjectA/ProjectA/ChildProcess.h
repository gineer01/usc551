//
//  ChildProcess.h
//  ProjectA
//
//  Created by Cuong Dong on 9/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectA_ChildProcess_h
#define ProjectA_ChildProcess_h

#include "Unp.h"

int runClientProcess(in_port_t port);

#endif

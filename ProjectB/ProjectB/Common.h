//
//  Common.h
//  ProjectB
//
//  Created by Cuong Dong on 10/31/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Common_h
#define ProjectB_Common_h


#define SETUP_MESSAGE 256
#define MAX_NAME 85
#define MAX_FILE_NAME 100
#define MAX_CLIENT 150
#define MAX_DATA 150
#define FIRST_CLIENT 0
#define FIRST_CLIENT_PORT 0
#define BUFFER_SIZE 256
#define BUFFER_SIZE_XL 1024

#define MANAGER_SLEEP 60
#define CLIENT_CHECK_SUCCESSOR_INTERVAL 10
#define CLIENT_WAIT 2
#define CLIENT_INTERVAL 1

typedef unsigned int HashId;

typedef struct {
	HashId hash;
	int port;
} NodeInfo;

typedef enum {
	SUCCESSOR_Q = 1,
	SUCCESSOR_R,
	PREDECESSOR_Q,
	PREDECESSOR_R,
	UPDATE_Q,
	UPDATE_R,
	STORE_PLEASE_Q,
	STORE_PLEASE_R,
	STORES_Q = 17,
	STORES_R,
	LEAVING_Q = 21,
	LEAVING_R,
	NEXT_DATA_Q,
	NEXT_DATA_R,
	UPDATE_LEAVING_Q,
	UPDATE_LEAVING_R,
	HELLO_PREDECESSOR_Q = 31,
	HELLO_PREDECESSOR_R
} MessageType;

#define START_COMMAND "start_client"
#define TKCC_COMMAND "start_tkcc"
#define STORE_COMMAND "store"
#define SEARCH_COMMAND "search"
#define END_COMMAND "end_client"
#define KILL_COMMAND "kill_client"

#define TCP_OK "ok\n"

#define STORE_FAIL 0
#define STORE_SUCCESS 1
#define STORE_FOUND 2

#define STORES_YES 1
#define STORES_NO 0

#define MAX_FINGER 33

#define NUMBER_OF_COPIES 4
#define NUMBER_OF_SEARCH 2
#define MESSAGE_LENGTH 85

#endif

//
//  Node.h
//  ProjectB
//
//  Created by Cuong Dong on 10/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Node_h
#define ProjectB_Node_h

#include <stdbool.h>
#include "Common.h"
#include "Kernel.h"


#define SUCCESSOR_INDEX 1
#define PREDECESSOR_INDEX 0
#define DOUBLE_SUCCESSOR_INDEX 101


HashId triadHash(uint32_t nonce, char *name);



typedef struct {
	NodeInfo node;
	NodeInfo firstNode;
	
	int stage;
	int nonce;
	bool isTkcc;
	
	Kernel *k;
	
	NodeInfo fingers[MAX_FINGER];
	NodeInfo doubleSuccessor;
	struct timeval checkSuccessor;
	
	HashId data[MAX_DATA];
	char *dataS[MAX_DATA];
} Node;


void initNode(Node *thisNode, int stage, bool isTkcc, uint32_t nonce, char *name, char *fs, int fp, int udpPort, Kernel *k);

void updateFinger(Node *thisNode, int i, NodeInfo info);
int saveDataWithoutValidation(Node *thisNode, char *s, HashId dataId);
int storeData(Node *thisNode, HashId dataId, char *s);
bool containsData(Node *thisNode, HashId data);
int findNextData(Node *thisNode, int queryId);


NodeInfo closestPrecedingFinger(Node *thisNode, HashId id);
#endif

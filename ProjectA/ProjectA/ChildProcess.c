//
//  ChildProcess.c
//  ProjectA
//
//  Created by Cuong Dong on 9/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "Unp.h"
#include "Util.h"

int runClientProcess(in_port_t port){
//	printf("Server Port: %d\n", port);
	
	int socket = Socket(AF_INET, SOCK_STREAM, 0);
	
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(0x7F000001);//127.0.0.1
	address.sin_port = htons(port);
	socklen_t len = sizeof(address);
	Connect(socket, (SA *) &address, len);
	
	const int BUFFER_SIZE = 256;
	char buffer[BUFFER_SIZE];
	int total = readSocket(socket, buffer, BUFFER_SIZE);
	buffer[total] = 0;
//	printf("Nonce received: %s", buffer);
	int nonce = atoi(buffer);
	
	pid_t id = getpid();
	int x = id + nonce;
	
	char clientInfo[256];
	snprintf(clientInfo, 256, "%d %d\n", x, id);
//	printf("client info: %s", clientInfo);
	Write(socket, clientInfo, strlen(clientInfo));
	Close(socket);
	
	return 0;
}
//
//  ManagerProcess.h
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_ManagerProcess_h
#define ProjectB_ManagerProcess_h

#include <stdio.h>
#include <sys/types.h>
#include "Common.h"
#include "Config.h"

in_port_t openTcpPort(int listeningSocket);

int setupClient(int listeningSocket, FILE *log, char name[MAX_NAME], char firstClientName[MAX_NAME], const int count, uint32_t nonce, int *firstUdpPort);

FILE * openManagerLogFile(int stage);

void sendCommandToClient(int* clientSockets, char *command, char *name);

void terminateClient(int* clientSockets, char names[][MAX_NAME], Command *command);
#endif

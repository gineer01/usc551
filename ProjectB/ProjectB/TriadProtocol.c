//
//  Triad.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <sys/types.h>
#include <sys/time.h>
#include "TriadProtocol.h"
#include "Util.h"
#include "Algorithm.h"



void respondSuccessor(Node *thisNode, Message *query, int port){
	Message *reply = malloc(sizeof(Message));
	
	reply->type = SUCCESSOR_R;
	reply->receiverId = query->receiverId;
	reply->nodeId = thisNode->fingers[SUCCESSOR_INDEX].hash;
	reply->port = thisNode->fingers[SUCCESSOR_INDEX].port;
	
	queueOutbound(port, reply, thisNode->k);
}

void respondPredecessor(Node *thisNode, Message *query, int port){
	Message *reply = malloc(sizeof(Message));
	
	reply->type = query->type == HELLO_PREDECESSOR_Q ? HELLO_PREDECESSOR_R : PREDECESSOR_R;
	reply->receiverId = query->receiverId;
	reply->nodeId = thisNode->fingers[PREDECESSOR_INDEX].hash;	
	reply->port = thisNode->fingers[PREDECESSOR_INDEX].port;
	
	queueOutbound(port, reply, thisNode->k);
}

void respondNextData(Node *thisNode, Message *query, int port){
	Message *reply = malloc(sizeof(Message));
	
	reply->type = NEXT_DATA_R;
	reply->receiverId = query->receiverId;
	reply->queryId = query->queryId;
	
	int index = findNextData(thisNode, query->queryId);
	if (index < 0){
		reply->length = 0;
	}
	else {
		reply->nodeId = thisNode->data[index];
		reply->length = (unsigned int) strlen(thisNode->dataS[index]);
		strcpy(reply->s, thisNode->dataS[index]);
		
		free(thisNode->dataS[index]);
		thisNode->dataS[index] = NULL;
		thisNode->data[index] = 0;
	}
	queueOutbound(port, reply, thisNode->k);
}

void respondStores(Node *thisNode, Message *query, NodeInfo found, int port){
	Message *reply = malloc(sizeof(Message));

	reply->type = STORES_R;
	reply->receiverId = thisNode->node.hash;
	reply->queryId = query->queryId;
	reply->nodeId = found.hash;
	reply->port = found.port;

	reply->successCode = ((reply->nodeId == thisNode->node.hash) && containsData(thisNode, reply->queryId)) ? STORES_YES : STORES_NO;
	
	if (reply->successCode == STORES_YES){
		for (int i = 0; i < MAX_DATA; i++) {
			if (thisNode->data[i] == reply->queryId){
				reply->length = (int) strlen(thisNode->dataS[i]);
				strcpy(reply->s, thisNode->dataS[i]);
			}
		}
	}
	else {
		reply->length = 0;
		strcpy(reply->s, "");
	}
	
	bool isAdded = queueOutbound(port, reply, thisNode->k);
	if (!isAdded){
		free(reply);
		fprintf(stderr, "SEVERE: Cannot queue outbound message. Increase outbound queue size?\n");
	}
}



void startFindSuccessor(Node *thisNode, Message *query, int port){
	HashId queryId = query->queryId;
	
	NodeInfo nodeToAsk = closestPrecedingFinger(thisNode, queryId);
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = STORES_Q;
	newQuery->receiverId = nodeToAsk.hash;
	newQuery->queryId = queryId;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, nodeToAsk.hash, STORES_R, FIND_SUCCESSOR_2, 1, NULL);
	context->local.returnPort = port;
	context->local.queryId = queryId;
	
    queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, context);
	printf("Node %08x findSuccessor for hash %08x by asking node %08x\n", thisNode->node.hash, queryId, nodeToAsk.hash);
}

void findSuccess2Step1(Node *thisNode, Message *m, int port){	
	NodeInfo found = {m->nodeId, m->port};
	respondStores(thisNode, m, found, port);
}

/*
 0:
 n' = n.closest_preceding_finger; //local call
 temp = n'.find_successor(id); //send stores-q to n'
 1:
 return temp;
 */
void continueFindSuccessor(Node *thisNode, Message *m, int step, int returnPort){
	switch (step) {	
		case 1:
			printf("Step 1 in find_successor\n");
			findSuccess2Step1(thisNode, m, returnPort);
			break;
			
		default:
			printError("Unexpected step number");
			break;
	}
}


/*
 
 if (id in (n, n.successor))
   return n.successor //local call
 else {
   0:
   n' = n.closest_preceding_finger; //local call
   temp = n'.find_successor(id); //send stores-q to n'
   1:
   return temp;
 }
 */

void findSuccesor(Node *thisNode, Message *query, int port){
	HashId queryId = query->queryId;	
	HashId nodeId = thisNode->node.hash;
	HashId successorId = thisNode->fingers[SUCCESSOR_INDEX].hash;
	HashId predecessorId = thisNode->fingers[PREDECESSOR_INDEX].hash;
	
	if ((nodeId == successorId) || isInRangeLeftOpen(queryId, predecessorId, nodeId)){
		printf("findSuccessor step 1. Found. nodeid %08x , successorId %08x, queryId %08x, prev %08x\n",
			   nodeId, successorId, queryId, predecessorId);
		respondStores(thisNode, query, thisNode->node, port);
	}
	else if (isInRangeLeftOpen(queryId, nodeId, successorId)){
		respondStores(thisNode, query, thisNode->fingers[SUCCESSOR_INDEX], port);
	}
	else {
		startFindSuccessor(thisNode, query, port);
	}
}

void getDataFromPrev(Node *thisNode, Message *query, int port){
	HashId queryId = query->queryId;
	
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = NEXT_DATA_Q;
	newQuery->receiverId = queryId;
	newQuery->queryId = queryId;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, queryId, NEXT_DATA_R, GET_ALL_DATA, 1, NULL);
	context->local.returnPort = port;
	
    queueMessageAndContext(thisNode->k, newQuery, port, context);
}

void continueContext(Node *thisNode, Message *m, ExecContext *ec){
	switch (ec->algo) {
		case FIND_SUCCESSOR_2:
			continueFindSuccessor(thisNode, m, ec->step, ec->local.returnPort);
			break;
			
		case APP_STORE:
			continueAppStore(thisNode, m, ec->step, ec);
			break;
			
		case APP_SEARCH:
			continueAppSearch(thisNode, m, ec->step, ec);
			break;
			
		case GET_ALL_DATA:
			continueGetData(thisNode, m, ec->step, ec);
			break;
		
		case HELLO_PREDECESSOR:
			continueHello(thisNode, m, ec->step, ec);
			break;
			
		case DO_NOTHING://do nothing
			printf("Do nothing algorithm\n");
			break;
			
		default:
			fprintf(stderr, "Cannot find matching algorithm for %d\n", ec->algo);
			break;
	}
}


void sendUpdateToPredecessor(Node *thisNode, int i, HashId hash, int port){
	if (i == PREDECESSOR_INDEX) return;
	
	Message *newQuery = malloc(sizeof(Message));
	NodeInfo nodeToAsk = thisNode->fingers[PREDECESSOR_INDEX];
	newQuery->type = UPDATE_Q;
	newQuery->receiverId = nodeToAsk.hash;
	newQuery->i = i;
	newQuery->nodeId = hash;
	newQuery->port = port;
	
    queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, NULL);
}

#define UPDATE_SUCCESS 1
#define UPDATE_FAILURE 0

int doNormalUpdate(Node *thisNode, NodeInfo newInfo, int i) {
    HashId nodeId = thisNode->node.hash;
	HashId newId = newInfo.hash;
	HashId fingerId = thisNode->fingers[i].hash;
	if (i > 0){
		if (nodeId == fingerId){
			HashId fingerStart = getFingerStart(thisNode->node.hash, i);
            //			printf("%08x update with fingerStart: fingerStart %08x newId %08x\n", nodeId, fingerStart, newId);
			if (!isInRangeLeftOpen(fingerStart, nodeId, newId)){
				return UPDATE_FAILURE;
			}
		}
		else if (!isInRangeRightOpen(newId, nodeId, fingerId)){
            //			printf("%08x update finger %d ignored: fingerId %08x newId %08x\n", nodeId, i, fingerId, newId);
			return UPDATE_FAILURE;
		}
	}	
	thisNode->fingers[i] = newInfo;
    //	printf("%08x update finger %d to %08x\n", nodeId, i, newId);
	if ((i == SUCCESSOR_INDEX) && (thisNode->stage == 6)){
		NodeInfo newNode = thisNode->fingers[SUCCESSOR_INDEX];
		sendUpdateToPredecessor(thisNode, DOUBLE_SUCCESSOR_INDEX, newNode.hash, newNode.port);
	}
	
	return UPDATE_SUCCESS;
}

int doUpdate(Node *thisNode, Message *query){
	//validation
	int i = query->i;
	NodeInfo newInfo = {query->nodeId, query->port};
	
	if (i == DOUBLE_SUCCESSOR_INDEX){
		printf("%08x double-successor is updated to %08x\n", thisNode->node.hash, newInfo.hash);
		thisNode->doubleSuccessor = newInfo;
		return UPDATE_SUCCESS;
	}
	else {
		return doNormalUpdate(thisNode, newInfo, i);
	}
}

void respondUpdate(Node *thisNode, Message *query, int port){
	Message *reply = malloc(sizeof(Message));
	
	reply->type = UPDATE_R;
	reply->receiverId = query->receiverId;
	reply->nodeId = query->nodeId;
	reply->port = query->port;
	reply->i = query->i;
	reply->successCode = doUpdate(thisNode, query);
	queueOutbound(port, reply, thisNode->k);
	
	bool specialIndex = (query->i == DOUBLE_SUCCESSOR_INDEX) || (query->i == PREDECESSOR_INDEX);
	if (reply->successCode == UPDATE_SUCCESS && !specialIndex){
		sendUpdateToPredecessor(thisNode, query->i, query->nodeId, query->port);
	}
	else {
//		fprintf(stderr, "Update ignored. %d\n", query->i);
//		logMessage(stderr, query, false);
	}
}

void respondUpdateLeaving(Node *thisNode, Message *query, int port){	
	if (thisNode->stage >= 5){
		Message *reply = malloc(sizeof(Message));
		
		reply->type = UPDATE_LEAVING_R;
		reply->receiverId = query->receiverId;
		reply->nodeId = query->nodeId;
		reply->port = query->port;
		reply->queryId = query->queryId;
		
		queueOutbound(port, reply, thisNode->k);
	}
	
	if (thisNode->stage == 6){
		NodeInfo nodeToSend = thisNode->fingers[PREDECESSOR_INDEX];
		Message *reply = malloc(sizeof(Message));
		
		reply->type = UPDATE_LEAVING_Q;
		reply->receiverId = nodeToSend.hash;
		reply->nodeId = query->nodeId;
		reply->port = query->port;
		reply->queryId = query->queryId;
		
		queueOutbound(nodeToSend.port, reply, thisNode->k);
	}
	
	for(int i = SUCCESSOR_INDEX; i < MAX_FINGER; i++){
		if (thisNode->fingers[i].hash == query->queryId){
			thisNode->fingers[i].hash = query->nodeId;
			thisNode->fingers[i].port = query->port;
		}
	}	
}


int doStorePlease(Node *thisNode, Message *query){
	query->s[query->length] = 0; //null-terminate
	HashId dataId = query->queryId; //the hash where the string is stored
	
	return storeData(thisNode, dataId, query->s);
}

void respondStorePlease(Node *thisNode, Message *query, int port){
	Message *reply = malloc(sizeof(Message));
	
	reply->type = STORE_PLEASE_R;
	reply->receiverId = query->receiverId;
	reply->length = query->length;
	strcpy(reply->s, query->s);
	
	reply->successCode = doStorePlease(thisNode, query);
	
	queueOutbound(port, reply, thisNode->k);
}


void validateQuery(Message *message, Node *thisNode) {
    if (thisNode->node.hash != message->receiverId){
		fprintf(stderr, "This node id : %08x\n", thisNode->node.hash);
		logMessage(stderr, message, false);
		printError("The receiver ID doesn't match the node ID");
	}
}

void respond(App *app, Node *thisNode, Message *message, int port){
//	printf("%08x responds to Triad message from port %d: ", thisNode->node.hash, port);
	logMessage(stdout, message, false);
	
	int context;
	switch (message->type) {
		case SUCCESSOR_Q:
			validateQuery(message, thisNode);
			respondSuccessor(thisNode, message, port);
			break;
			
		case PREDECESSOR_Q:
		case HELLO_PREDECESSOR_Q:
			validateQuery(message, thisNode);
			respondPredecessor(thisNode, message, port);
			break;
			
		case NEXT_DATA_Q:
			validateQuery(message, thisNode);
			respondNextData(thisNode, message, port);
			break;
			
		case STORES_Q:
			validateQuery(message, thisNode);
			findSuccesor(thisNode, message, port);
			break;
			
		case LEAVING_Q:
			validateQuery(message, thisNode);
			getDataFromPrev(thisNode, message, port);
			break;
			
		case UPDATE_Q:
			validateQuery(message, thisNode);
			respondUpdate(thisNode, message, port);
			break;
			
		case UPDATE_LEAVING_Q:
			if (thisNode->stage == 6){
				context = checkContext(thisNode->k, message);

				if (context >= 0) {
					ExecContext *ec = thisNode->k->contexts[context];
					continueContext(thisNode, message, ec);
					freeContext(thisNode->k, context);
					break;
				}
			}
			validateQuery(message, thisNode);
			respondUpdateLeaving(thisNode, message, port);
			break;
			
		case STORE_PLEASE_Q:
			respondStorePlease(thisNode, message, port);
			break;
			
		case STORES_R:
		case UPDATE_R:
		case STORE_PLEASE_R:
		case SUCCESSOR_R:
		case PREDECESSOR_R:
		case HELLO_PREDECESSOR_R:
		case NEXT_DATA_R:
		case UPDATE_LEAVING_R:
			//All replies should have some context of why a query has been sent
			// and what should be done with the reply
			context = checkContext(thisNode->k, message);
			if (context < 0) {
				if (message->type == UPDATE_R) return;//expected
				if (message->type == UPDATE_LEAVING_R) return;//expected
				
				fprintf(stderr, "WARNING: Cannot find matching context for a reply message: ");
				logMessage(stderr, message, false);
				return;
			}	else {
				ExecContext *ec = thisNode->k->contexts[context];
				continueContext(thisNode, message, ec);
				freeContext(thisNode->k, context);
			}
			break;
			
		case LEAVING_R:
			triadLeave(thisNode);
			app->timeToDie = true;
			break;
		default:
			printf("Cannot respond to the query type %d\n", message->type);
			break;
	}
}

void expireContext(Node *thisNode, ExecContext *ec){
	switch (ec->algo) {
		case HELLO_PREDECESSOR:
			expireHelloPredecessor(thisNode);
			break;
			
		default:
			fprintf(stderr, "Cannot find matching algorithm for %d\n", ec->algo);
			break;
	}
}

void sendCheckSuccessorMessage(Node *node){
	NodeInfo nodeToAsk = node->fingers[SUCCESSOR_INDEX];
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = HELLO_PREDECESSOR_Q;
	newQuery->receiverId = nodeToAsk.hash;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, nodeToAsk.hash, HELLO_PREDECESSOR_R, HELLO_PREDECESSOR, 1, NULL);
	
	queueMessageAndContext(node->k, newQuery, nodeToAsk.port, context);
}

void checkTimeout(Node *node){
	struct timeval time;
	gettimeofday(&time, NULL);
	
	Kernel *k = node->k;
	for (int i = 0; i < MAX_CLIENT; i++){
		if (k->contexts[i] == NULL) continue;
		
		ExecContext *ec = k->contexts[i];
		if (ec->expireAt.tv_sec <= time.tv_sec){
			expireContext(node, ec);
			freeContext(k, i);
		}
	}
	
	if (node->checkSuccessor.tv_sec <= time.tv_sec){
		sendCheckSuccessorMessage(node);
		node->checkSuccessor.tv_sec = time.tv_sec + CLIENT_CHECK_SUCCESSOR_INTERVAL;
	}
}


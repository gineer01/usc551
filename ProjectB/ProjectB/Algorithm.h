//
//  Algorithm.h
//  ProjectB
//
//  Created by Cuong Dong on 11/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Algorithm_h
#define ProjectB_Algorithm_h

#include "App.h"

void continueAppStore(Node *thisNode, Message *m, int step, ExecContext *ec);
void continueAppSearch(Node *thisNode, Message *m, int step, ExecContext *ec);
void continueGetData(Node *thisNode, Message *m, int step, ExecContext *ec);
void continueHello(Node *thisNode, Message *m, int step, ExecContext *ec);

void expireHelloPredecessor(Node *node);

//Synchronous (blocking) functions
NodeInfo rpcFindSuccessor(Node *thisNode, NodeInfo *remote, HashId hash, int *successCode);
NodeInfo rpcPredecessor(Node *thisNode, NodeInfo *remote);
NodeInfo rpcSuccessor(Node *thisNode, NodeInfo *remote);
int rpcUpdate(Node *thisNode, NodeInfo *remote, int i, NodeInfo *newInfo);
int rpcUpdateLeaving(Node *thisNode, NodeInfo *remote, NodeInfo *newInfo, HashId leavingNode);

//Synchronous algorithm (when leaving/joining, node can't respond to any message)
void triadLeave(Node *thisNode);
void triadJoin(Node *thisNode);
#endif

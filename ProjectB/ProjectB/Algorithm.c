//
//  Algorithm.c
//  ProjectB
//
//  Created by Cuong Dong on 11/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <sys/time.h>
#include <stdio.h>
#include "Algorithm.h"
#include "Util.h"

void appStoreSuccess(App *app, Message *m, Node *thisNode, ExecContext *ec) {
    if (thisNode->stage == 7){
		app->count++;
		app->isSendOk = app->count == NUMBER_OF_COPIES;
	}
	else {
		app->isSendOk = true;
	}
    HashId hash = ec->local.storeId;
	
    fprintf(thisNode->k->log, "add %s with hash 0x%08x to node 0x%08x\n", m->s, hash, m->receiverId);
    fprintf(stdout, "add %s with hash 0x%08x to node 0x%08x\n", m->s, hash, m->receiverId);
    return;
}

void appStoreAlgorithmStep1(Node *thisNode, Message *m, ExecContext *ec){
	//if found, do not need to send STORE-R
	if (m->successCode == STORES_YES){
		App *app = ((App *)ec->local.app);
		appStoreSuccess(app, m, thisNode, ec);
		return;
	}
	
	//send Store-r to m->nodeId;
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = STORE_PLEASE_Q;
	newQuery->queryId = ec->local.storeId;
	newQuery->receiverId = m->nodeId;
	newQuery->length = (unsigned int) strlen(ec->local.name);
	memcpy(newQuery->s, ec->local.name, newQuery->length);
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, m->nodeId, STORE_PLEASE_R, APP_STORE, 2, NULL);
	context->local.app = ec->local.app;
	context->local.storeId = ec->local.storeId;
	printf("Storing %s with hash %08x at node %08x\n", ec->local.name, ec->local.storeId, m->nodeId);
	
	queueMessageAndContext(thisNode->k, newQuery, m->port, context);
}

void appStoreAlgorithmStep2(Node *thisNode, Message *m, ExecContext *ec){
	if (m->successCode == STORE_SUCCESS || m->successCode == STORE_FOUND){
		App *app = ((App *)ec->local.app);
		appStoreSuccess(app, m, thisNode, ec);;
	}
	else {
		fprintf(stderr, "Store failed.\n");
		logMessage(stderr, m, false);
	}
}

/*
 0:
 (message, app)
 n' = stores(di)
 1:
 m = n'.store (message)
 2:
 app.sendOk
 */
void continueAppStore(Node *thisNode, Message *m, int step, ExecContext *ec){
	switch (step) {
		case 1:
			printf("Step 1 in app_store\n");
			appStoreAlgorithmStep1(thisNode, m, ec);
			break;
			
		case 2:
			printf("Step 2 in app_store\n");
			appStoreAlgorithmStep2(thisNode, m, ec);
			break;
			
		default:
			printError("Unexpected step number");
			break;
	}
}


void appSearchSuccess(App *app, Message *m, Node *thisNode, char *name, int ith) {
	HashId nodeId = thisNode->node.hash;
	bool found = (m->successCode == STORES_YES);
	fprintf(thisNode->k->log, "search %s to node 0x%08x, key %s\n", name, nodeId,
			found ? "PRESENT" : "ABSENT");
	fprintf(stdout, "search %s to node 0x%08x, key %s\n", name, nodeId,
			found ? "PRESENT" : "ABSENT");
	
	app->searchDone[ith] = found ? PRESENT : ABSENT;
	app->searchNode[ith] = m->receiverId;
	if (found) {
		strcpy(app->search[ith], m->s);
	}
	
	searchDone(app, thisNode, name, ith);
    return;
}

void appSearchAlgorithmStep1(Node *thisNode, Message *m, ExecContext *ec){
	if (m->successCode == STORES_YES){
		App *app = ((App *)ec->local.app);
		appSearchSuccess(app, m, thisNode, ec->local.name, ec->local.searchAttempt);
		return;
	}
	
	Message *newQuery = malloc(sizeof(Message));	
	newQuery->type = STORES_Q;
	newQuery->receiverId = m->nodeId;
	newQuery->queryId = m->queryId;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, m->nodeId, STORES_R, APP_SEARCH, 2, ec->local.name);
	context->local.app = ec->local.app;
	context->local.searchAttempt = ec->local.searchAttempt;
	context->local.queryId = m->queryId;
	
	queueMessageAndContext(thisNode->k, newQuery, m->port, context);
}

void appSearchAlgorithmStep2(Node *thisNode, Message *m, ExecContext *ec){
	App *app = ((App *)ec->local.app);
	appSearchSuccess(app, m, thisNode, ec->local.name, ec->local.searchAttempt);
}

/*
 0:
 (message, app)
 n' = stores(di)
 1:
 m = n'.stores(di)
 2:
 app.sendOk
 */
void continueAppSearch(Node *thisNode, Message *m, int step, ExecContext *ec){
	switch (step) {
		case 1:
			printf("Step 1 in app_search\n");
			appSearchAlgorithmStep1(thisNode, m, ec);
			break;
			
		case 2:
			printf("Step 2 in app_search\n");
			appSearchAlgorithmStep2(thisNode, m, ec);
			break;
			
		default:
			printError("Unexpected step number");
			break;
	}
}

/*
 0:
 n' = get_data(di)
 1:
 if (n'.l = 0) send leaving-r
 else goto 0
 
 */
void continueGetData(Node *thisNode, Message *m, int step, ExecContext *ec){
	switch (step) {
		case 1:
			if (m->length == 0){
				printf("Got all data. You can die now!\n");
				for(int i = 0; i < MAX_DATA; i++){
					if (thisNode->dataS[i] == NULL) continue;
					printf("Data %d hash %08x : %s\n", i, thisNode->data[i], thisNode->dataS[i]);
				}
				
				Message *newQuery = malloc(sizeof(Message));
				newQuery->type = LEAVING_R;
				newQuery->receiverId = thisNode->node.hash;
				
				queueMessageAndContext(thisNode->k, newQuery, ec->local.returnPort, NULL);
			} else {
				if (saveDataWithoutValidation(thisNode, m->s, m->nodeId) != STORE_SUCCESS){
					fprintf(stderr, "Error saving data '%s'. Queue is full?\n", m->s);
				}
				
				Message *newQuery = malloc(sizeof(Message));
				newQuery->type = NEXT_DATA_Q;
				newQuery->receiverId = m->receiverId;
				newQuery->queryId = m->nodeId - 1;
				
				ExecContext *context = malloc(sizeof(ExecContext));
				initContext(context, m->receiverId, NEXT_DATA_R, GET_ALL_DATA, 1, NULL);
				context->local.returnPort = ec->local.returnPort;
				
				queueMessageAndContext(thisNode->k, newQuery, ec->local.returnPort, context);
			}
			break;
	}
}

void repairRing(Node *node, NodeInfo response){
	HashId nodeId = node->node.hash;
	NodeInfo successor = node->fingers[SUCCESSOR_INDEX];
	
	if (isInRangeOpen(response.hash, nodeId, successor.hash)) {
		updateFinger(node, SUCCESSOR_INDEX, response);
		node->doubleSuccessor = successor;
		
		int m = getMaxFinger(node->stage);
		for (int i = 1; i < m; i++) {
			int start = getFingerStart(nodeId, i + 1);
			if (isInRangeRightOpen(start, nodeId, node->fingers[i].hash)){
				updateFinger(node, i + 1, node->fingers[i]);
			}
			else {
				break;
			}
		}
		fprintf(node->k->log, "hello-predecessor-r causes repair of my links.\n");
		fprintf(stderr, "hello-predecessor-r causes repair of my links.\n");
	}
	else if (isInRangeRightOpen(response.hash, successor.hash, nodeId)){
		Message *newQuery = malloc(sizeof(Message));
		
		newQuery->type = UPDATE_Q;
		newQuery->receiverId = successor.hash;
		newQuery->i = PREDECESSOR_INDEX;
		newQuery->nodeId = nodeId;
		newQuery->port = node->node.port;
		
		queueOutbound(successor.port, newQuery, node->k);
		
		fprintf(node->k->log, "hello-predecessor-r causes me to repair my successor’s predecessor.\n");
		fprintf(stderr, "hello-predecessor-r causes me to repair my successor’s predecessor.\n");
	}
	else {
		fprintf(stderr, "Impossible case in repairRing() function\n");
	}
}

/*
 0:
 m = hello(di)
 1:
 check m; return;
 2:
 update double successor
 3:
 do nothing
 
 */
void continueHello(Node *thisNode, Message *m, int step, ExecContext *ec){
	HashId myId = thisNode->node.hash;
	NodeInfo newInfo = {m->nodeId, m->port};
	switch (step) {
		case 1:			
			if (m->nodeId == thisNode->node.hash){//All is well
				fprintf(thisNode->k->log, "hello-predecessor-r confirms my successor’s predecessor is me, 0x%08x\n", m->nodeId);
				fprintf(stdout, "hello-predecessor-r confirms my successor’s predecessor is me, 0x%08x\n", m->nodeId);
			} else {
				fprintf(thisNode->k->log, "hello-predecessor-r reports my successor's predecessor is 0x%08x, not me, 0x%08x\n", m->nodeId, myId);
				fprintf(stderr, "hello-predecessor-r reports my successor's predecessor is 0x%08x, not me, 0x%08x\n", m->nodeId, myId);
				repairRing(thisNode, newInfo);
			}
			break;
			
		case 2://respond to successor-q to get double-successor
			thisNode->doubleSuccessor = newInfo;
			break;

		default:
			printError("Unexpected step number");
			break;
	}
}

//Send message to the remote node and fill the response info into *node
//Return the success code (might be random if the response doesn't use success code)
int sendAndReceive(Node *thisNode, NodeInfo *remote, Message *message, MessageType responseType, NodeInfo *node) {
    fd_set readingSet, writingSet;
	int udpSocket = thisNode->k->udpSocket;
	int maxFd = udpSocket;
	FD_ZERO(&readingSet);
	FD_ZERO(&writingSet);
	FD_SET(udpSocket, &writingSet);
	FD_SET(udpSocket, &readingSet);
	
	Message *m = malloc(sizeof(Message));
	int success = -1;
	
	while (true) {
		fd_set tempRead = readingSet;
		fd_set tempWrite = writingSet;
		//		printf("Start listening in RPC at socket %d\n", thisNode->udpSocket);
		int nReady = select(maxFd+1, &tempRead, &tempWrite, NULL, NULL);
		
		if (nReady < 0){
			if (errno == EINTR) {
				continue;
			}
		}
		
		if (FD_ISSET(udpSocket, &tempWrite)){
			//			printf("Send RPC: "); logMessage(stdout, message, true);
			sendMessage(thisNode->k, message, udpSocket, remote->port);
			
			FD_ZERO(&writingSet);
			
			if (--nReady <= 0) continue;
		}
		
		if (FD_ISSET(udpSocket, &tempRead)){
			
			receiveMessageSimple(thisNode->k, m, udpSocket);
			//			printf("RPC read : ");logMessage(stdout, m, false);
			
			if ((m->receiverId == remote->hash) && (m->type == responseType)){
                node->hash = m->nodeId;
				node->port = m->port;
				success = m->successCode;
				break;
			}
			else {
				//otherwise, ignore the message because this node is not ready yet
				fprintf(stderr, "%08x receiving Triad message when not ready. Expecting response from %08x of type %d. Instead %d\n", m->receiverId, remote->hash, responseType, m->type);
				
			}
			
			if (--nReady <= 0) continue;
		}
	}
	
	free(m);
	return success;
}

NodeInfo rpcPredecessor(Node *thisNode, NodeInfo *remote){
	Message m = {PREDECESSOR_Q, remote->hash};
	NodeInfo node;
	sendAndReceive(thisNode, remote, &m, PREDECESSOR_R, &node);
	return node;
}

NodeInfo rpcSuccessor(Node *thisNode, NodeInfo *remote){
	Message m = {SUCCESSOR_Q, remote->hash};
	NodeInfo node;
	sendAndReceive(thisNode, remote, &m, SUCCESSOR_R, &node);
	return node;
}

NodeInfo rpcFindSuccessor(Node *thisNode, NodeInfo *remote, HashId hash, int *successCode){
	Message m = {STORES_Q, remote->hash};
	m.queryId = hash;
	NodeInfo node;
	*successCode = sendAndReceive(thisNode, remote, &m, STORES_R, &node);
	return node;
}

int rpcUpdate(Node *thisNode, NodeInfo *remote, int i, NodeInfo *newInfo){
	Message m = {UPDATE_Q, remote->hash};
	m.i	= i;
	m.nodeId = newInfo->hash;
	m.port = newInfo->port;
	
	NodeInfo node;
	return sendAndReceive(thisNode, remote, &m, UPDATE_R, &node);
}

int rpcUpdateLeaving(Node *thisNode, NodeInfo *remote, NodeInfo *newInfo, HashId leavingNode){
	Message m = {UPDATE_LEAVING_Q, remote->hash};
	m.nodeId = newInfo->hash;
	m.port = newInfo->port;
	m.queryId = leavingNode;
	
	NodeInfo node;
	return sendAndReceive(thisNode, remote, &m, UPDATE_LEAVING_R, &node);
}

void triadLeave(Node *thisNode){
	HashId nodeId = thisNode->node.hash;
	NodeInfo replacement = thisNode->fingers[SUCCESSOR_INDEX];
	NodeInfo predecessor = thisNode->fingers[PREDECESSOR_INDEX];
	
	if (nodeId == replacement.hash) return; //only node
	
	rpcUpdate(thisNode, &replacement, PREDECESSOR_INDEX, &predecessor);
	HashId start = predecessor.hash;
	do {
		rpcUpdateLeaving(thisNode, &predecessor, &replacement, nodeId);
		predecessor = rpcPredecessor(thisNode, &predecessor);
	} while (predecessor.hash != start);
}


//This fuction returns n - 2^(i-1) mod 2^32
HashId getPrevFinger(Node *thisNode, int i){
	HashId nodeId = thisNode->node.hash;
	unsigned long power = (1ul<<(i-1));
	if (nodeId < power){
		unsigned long t = 0xFFFFFFFF - power + 1;
		return (HashId)(nodeId + t);
	}
	return nodeId - (HashId)power;
}

//Stoica paper: figure 6
void updateOthers(Node *thisNode){
	printf("%08x starts updateOThers()\n", thisNode->node.hash);
	int m = getMaxFinger(thisNode->stage);
	NodeInfo predecessor = thisNode->fingers[PREDECESSOR_INDEX];
	NodeInfo prevSuc = {0, 0};
	NodeInfo cache;
	NodeInfo successor = {0, 0};
	NodeInfo nodeToUpdateFinger[MAX_FINGER];
	
	for (int i = 1; i <= m; i++) {
		int dontCare = 0;
		//Find the hash of the node whose i-th finger might be n. That node is predecessor(n - 2^(i-1))
		HashId hash = getPrevFinger(thisNode, i);
		
		
		//Find the node to update: it is predecessor of findSuccessor(hash)
		//Optimization: if it is this node, use predecessor without sending message to this node itself
		if (isInRangeLeftOpen(hash, predecessor.hash, thisNode->node.hash)){
			nodeToUpdateFinger[i] = predecessor;
		} else {
			successor = rpcFindSuccessor(thisNode, &thisNode->firstNode, hash, &dontCare);
			//Optimization: if it is the same as the last node, use the cache
			if (prevSuc.hash == successor.hash){
				nodeToUpdateFinger[i] = cache;
			}
			else {
				nodeToUpdateFinger[i] = rpcPredecessor(thisNode, &successor);
			}
		}
		//		printf("UpdateOthers: predecessor %08x of hash %08x\n", p.hash, hash);
		//		printf("UpdateOthers: send update to %08x, i = %d, node = %08x\n", predecessor.hash, i, thisNode->node.hash);
		prevSuc = successor;
		cache = nodeToUpdateFinger[i];
	}
	
	for (int i = 1; i <= m; i++) {
		//Don't update itself
		if (nodeToUpdateFinger[i].hash != thisNode->node.hash){
			rpcUpdate(thisNode, nodeToUpdateFinger + i, i, &thisNode->node);
		}
	}
	printf("%08x ends updateOThers()\n", thisNode->node.hash);
}

void updateDoubleSuccessor(Node *thisNode){
	NodeInfo successor = thisNode->fingers[SUCCESSOR_INDEX];
	if (thisNode->node.hash == successor.hash){ //only one node in ring
		thisNode->doubleSuccessor = successor;
	}
	else {
		thisNode->doubleSuccessor = rpcSuccessor(thisNode, &successor);
	}
	
	printf("%08x double-successor is %08x\n", thisNode->node.hash, thisNode->doubleSuccessor.hash);
}

void initFingerTable(Node *thisNode){
	int success = 0;
	HashId nodeId = thisNode->node.hash;
	NodeInfo successor = rpcFindSuccessor(thisNode, &thisNode->firstNode, getFingerStart(thisNode->node.hash, SUCCESSOR_INDEX), &success);
	printf("\nINFO: %08x Successor is : %08x\n", nodeId, successor.hash);
	updateFinger(thisNode, SUCCESSOR_INDEX, successor);
	if (thisNode->stage == 6){
		updateDoubleSuccessor(thisNode);
	}
	
	NodeInfo predecessor = rpcPredecessor(thisNode, &successor);
	updateFinger(thisNode, PREDECESSOR_INDEX, predecessor);
	
	rpcUpdate(thisNode, &successor, PREDECESSOR_INDEX, &thisNode->node);
	
	if (thisNode->stage == 6){
		printf("%08x sending update double-successor to %08x. New DS is %08x\n", thisNode->node.hash, predecessor.hash, successor.hash);
		rpcUpdate(thisNode, &predecessor, DOUBLE_SUCCESSOR_INDEX, &successor);
	}
	
	int m = getMaxFinger(thisNode->stage);
	for (int i = 1; i < m; i++) {
		int start = getFingerStart(thisNode->node.hash, i + 1);
		if (isInRangeRightOpen(start, nodeId, thisNode->fingers[i].hash)){
			updateFinger(thisNode, i + 1, thisNode->fingers[i]);
		}
		else {
			NodeInfo successor = rpcFindSuccessor(thisNode, &thisNode->firstNode, start, &success);
			updateFinger(thisNode, i + 1, successor);
		}
	}
	printf("\nINFO : initFingerTable completed for %08x. Successor is : %08x\n\n", nodeId, successor.hash);
}


void triadJoin(Node *thisNode){
	printf("**** %08x with port %d joining Triad ring ****\n", thisNode->node.hash, thisNode->node.port);
	if (thisNode->firstNode.hash == thisNode->node.hash){
		int m = getMaxFinger(thisNode->stage);
		for(int i = 0; i <= m; i++){
			updateFinger(thisNode, i, thisNode->node);
		}
	}
	else if (thisNode->stage < 4){
		int dontCare = 0;
		NodeInfo successor = rpcFindSuccessor(thisNode, &thisNode->firstNode, getFingerStart(thisNode->node.hash, SUCCESSOR_INDEX), &dontCare);
		printf("\n\nINFO : Successor for %08x is : %08x\n\n", thisNode->node.hash, successor.hash);
		updateFinger(thisNode, SUCCESSOR_INDEX, successor);
		
		NodeInfo predecessor = rpcPredecessor(thisNode, &successor);
		updateFinger(thisNode, PREDECESSOR_INDEX, predecessor);
		rpcUpdate(thisNode, &successor, PREDECESSOR_INDEX, &thisNode->node);
		//-- end of init_finger_table in Stoica paper
		
		//-- start of update_others in Stoica paper
		rpcUpdate(thisNode, &predecessor, SUCCESSOR_INDEX, &thisNode->node);
	}
	else {
		initFingerTable(thisNode);
		updateOthers(thisNode);
	}
}

void updateOthersUnreponsiveNode(Node *node, HashId unresponsiveNode) {
    NodeInfo nodeToAsk = node->fingers[PREDECESSOR_INDEX];
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = UPDATE_LEAVING_Q;
	newQuery->receiverId = nodeToAsk.hash;
	newQuery->nodeId = node->doubleSuccessor.hash;
	newQuery->port = node->doubleSuccessor.port;
	newQuery->queryId = unresponsiveNode;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, nodeToAsk.hash, UPDATE_LEAVING_Q, DO_NOTHING, 0, NULL);
	queueMessageAndContext(node->k, newQuery, nodeToAsk.port, context);
	fprintf(node->k->log, "hello-predecessor-q triggers node failure notification\n");
	fprintf(stdout, "hello-predecessor-q triggers node failure notification\n");
	printf("Node %08x starts updating other about non-responsive node %08x\n", node->node.hash, unresponsiveNode);
}

void getDoubleSuccessor(Node *node) {
    NodeInfo nodeToAsk = node->doubleSuccessor;
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = SUCCESSOR_Q;
	newQuery->receiverId = nodeToAsk.hash;
	
	ExecContext *context = malloc(sizeof(ExecContext));
	initContext(context, nodeToAsk.hash, SUCCESSOR_R, HELLO_PREDECESSOR, 2, NULL);
	queueMessageAndContext(node->k, newQuery, nodeToAsk.port, context);
}

void updateSuccessor(Node *node) {
    NodeInfo nodeToAsk = node->doubleSuccessor;
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = UPDATE_Q;
	newQuery->receiverId = nodeToAsk.hash;
	newQuery->i = PREDECESSOR_INDEX;
	newQuery->nodeId = node->node.hash;
	newQuery->port = node->node.port;
	
	printf("Asking %08x to update predecessor to %08x due to no reply\n", nodeToAsk.hash, newQuery->nodeId);
	queueOutbound(nodeToAsk.port, newQuery, node->k);
}

void expireHelloPredecessor(Node *node){	
	HashId unresponsiveNode = node->fingers[SUCCESSOR_INDEX].hash;
	printf("%08x uses double-successor %08x as successor due to no-reply\n", node->node.hash, node->doubleSuccessor.hash);
	updateFinger(node, SUCCESSOR_INDEX, node->doubleSuccessor);
	for(int i = SUCCESSOR_INDEX; i < MAX_FINGER; i++){
		if (node->fingers[i].hash == unresponsiveNode){
			node->fingers[i] = node->doubleSuccessor;
		}
	}
	
	fprintf(node->k->log, "hello-predecessor-r non-reply: my successor is non-responsive\n");
	fprintf(stdout, "hello-predecessor-r non-reply: my successor %08x is non-responsive\n", unresponsiveNode);
	
	updateSuccessor(node);
	getDoubleSuccessor(node);
	updateOthersUnreponsiveNode(node, unresponsiveNode);
}

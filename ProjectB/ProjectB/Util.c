//
//  Util.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "Unp.h"
#include "Util.h"


//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

void fileError(const char *message) {
    perror(message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

int count(char *buffer, int length, char c){
	int count = 0;
	for (int i = 0; i < length; i++){
		if (buffer[i] == c) {
			count++;
		}
	}
	return count;
}

//Read a socket and ignore "Interrupted system call" until numOfLines are found
int readSocket(int socket, char *buffer, int bufferSize, int numOfLines){
	ssize_t n;
	int total = 0;
	buffer[total] = 0;
	do {
		n = read(socket, buffer + total, bufferSize - total);
		if (n > 0){
			total += n;
			buffer[total] = 0;
		}
		else {
			if (n == 0) return -1;
			
			if (errno == EINTR) {
				continue;
			}
			else {
				err_sys("read error");
			}
		}
	} while (count(buffer, total, '\n') < numOfLines);
	
	return total;
}

void logRawMessage(FILE *log, char *buffer, size_t size){
	fprintf(log, "raw: ");
	
	for (int i = 0; i < size; i++) {
		fprintf(log, "%02x", buffer[i]);
	}
	
	fprintf(log, "\n");
}

bool isInRangeLeftOpen(HashId queryId, HashId startExclusive, HashId endInclusive){
	if (startExclusive == endInclusive) return true;
	
	if (endInclusive > startExclusive){
		return (queryId > startExclusive) && (queryId <= endInclusive);
	}
	else {//wrap around
		bool betweenStart_0 = (queryId > startExclusive);
		bool between0_End = queryId <= endInclusive;
		return between0_End || betweenStart_0;
	}
}
bool isInRangeRightOpen(HashId queryId, HashId startInclusive, HashId endExclusive){
	if (endExclusive == startInclusive) return true;
	
	if (endExclusive > startInclusive){
		return (queryId >= startInclusive) && (queryId < endExclusive);
	}
	else {//wrap around
		bool betweenStart_0 = (queryId >= startInclusive);
		bool between0_End = queryId < endExclusive;
		return between0_End || betweenStart_0;
	}
}

bool isInRangeOpen(HashId queryId, HashId startExclusive, HashId endExclusive){
	if (endExclusive >= startExclusive){
		return (queryId > startExclusive) && (queryId < endExclusive);
	}
	else {//wrap around
		bool betweenStart_0 = (queryId > startExclusive);
		bool between0_End = queryId < endExclusive;
		return between0_End || betweenStart_0;
	}
}

HashId getFingerStart(HashId nodeId, int k){
	return (HashId)(nodeId + (1ul<<(k-1)));
}

int getMaxFinger(int stage){
	return (stage < 4) ? 1 : (MAX_FINGER - 1);
}
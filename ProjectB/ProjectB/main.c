//
//  main.c
//  ProjectB
//
//  Created by Cuong Dong on 10/27/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "CommandParser.h"
#include "Util.h"
#include "Config.h"
#include "ChildProcess.h"
#include "Unp.h"
#include "ManagerProcess.h"



int main(int argc, const char * argv[])
{
	CommandOptions options = parseCommandLine(argc, argv);
	FILE *input = openFile(options.path);
	Config config = readConfigFile(input);
	printf("Stage: %d\n", config.stage);
	printf("Nonce: %d\n", config.nonce);
	
	//open a listening socket
	int listeningSocket = Socket(AF_INET, SOCK_STREAM, 0);
	in_port_t port = openTcpPort(listeningSocket);
	
	//get stage number to pass to child process
	int stage = config.stage;
	
	//flush before forking; otherwise, children get the same
	//string in buffer and print the same thing multiple times
	fflush(stdout);
	
	Command command = {-1, NULL};
	char name[MAX_NAME];
	command.name = name;
	
	FILE *log = openManagerLogFile(stage);
	fprintf(log, "manager port: %d\n", port);
	printf("manager port: %d\n", port);	

	
	char names[MAX_CLIENT][MAX_NAME];
	int count = 0;
	
	//Manager process: setup client and send commands to clients
	int clientSockets[MAX_CLIENT];
	for (int i = 0; i < MAX_CLIENT; i++){
		clientSockets[i] = -1;
	}
	int firstUdpPort = 0;
	
	while (getNextCommand(input, &command)) {
		if (command.type == START || command.type == TKCC){
			bool isTkcc = command.type == TKCC;
			pid_t pid = Fork();
			if (pid == 0) {
				Close(listeningSocket);
				_exit(runClientProcess(port, stage, isTkcc));
			}
			else {
				strncpy(names[count], command.name, MAX_NAME);
				clientSockets[count] = setupClient(listeningSocket, log, command.name, names[FIRST_CLIENT], count, config.nonce, &firstUdpPort);
				count++;
			}
			continue;
		}
		
		switch (command.type) {
			case STORE:
				sendCommandToClient(clientSockets, STORE_COMMAND, command.name);
				break;
				
			case SEARCH:
				sendCommandToClient(clientSockets, SEARCH_COMMAND, command.name);
				break;
				
			case END:
			case KILL:
				terminateClient(clientSockets, names, &command);
				break;
				
			default:
				printf("Unrecognizable command\n");
				break;
		}
	}
	

	Close(listeningSocket);
	Fclose(input);
	Fclose(log);
    return 0;
}
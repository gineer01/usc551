//
//  ManagerProcess.c
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include "Unp.h"
#include "ManagerProcess.h"
#include "Util.h"

void readOk(int clientSocket)
{
    char buffer[BUFFER_SIZE];
    int total = readSocket(clientSocket, buffer, BUFFER_SIZE, 1);
    buffer[total] = 0;//null-terminate buffer
    
    if (strcmp(buffer, TCP_OK) != 0){
        fprintf(stderr, "Unexpected message received: %s\n", buffer);
        return;
    }
}

void sendCommand(char *command, char *name, int clientSocket)
{
    char message[BUFFER_SIZE];
    snprintf(message, BUFFER_SIZE, "%s\n%s\n", command, name);
    Write(clientSocket, message, strlen(message));
}

void sendCommandAndWait(char *command, char *name, int clientSocket)
{
	fd_set readingSet, writingSet;
	int maxFd = clientSocket;
	FD_ZERO(&readingSet);
	FD_ZERO(&writingSet);
	FD_SET(clientSocket, &writingSet);
	
	while (true) {
		fd_set tempRead = readingSet;
		fd_set tempWrite = writingSet;
		
		int nReady = select(maxFd+1, &tempRead, &tempWrite, NULL, NULL);
		
		if (nReady < 0){
			if (errno == EINTR) {
				continue;
			}
		}
		
		if (FD_ISSET(clientSocket, &tempWrite)){
			sendCommand(command, name, clientSocket);
			FD_CLR(clientSocket, &writingSet);
			FD_SET(clientSocket, &readingSet);
			if (--nReady <= 0) continue;
		}
		else if (FD_ISSET(clientSocket, &tempRead)){
			readOk(clientSocket);
			return;
		}
	}
}

in_port_t openTcpPort(int listeningSocket)
{
    struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	Bind(listeningSocket, (SA *) &address, sizeof(address));
	Listen(listeningSocket, MAX_CLIENT);
	
	//get port number
	socklen_t len = sizeof(address);
	memset(&address, 0, sizeof(address));
	Getsockname(listeningSocket, (SA *) &address, &len);
	in_port_t port = ntohs(address.sin_port);
    return port;
}

uint16_t readClientUpdPort(int connectedSocket, FILE *log) {
	char buffer[BUFFER_SIZE];
	int total = readSocket(connectedSocket, buffer, BUFFER_SIZE, 2);
	buffer[total] = 0;//null-terminate buffer
	printf("client says: %s", buffer);
	fprintf(log, "client says: %s", buffer);
		
	uint32_t n;
	uint16_t udpPort;
	sscanf(buffer, " %u %hu ", &n, &udpPort);
	return udpPort;
}

void sendSetupMessage(char *firstClientName, uint16_t firstClientPort, char *name, uint32_t nonce, int connectedSocket) {
    char setupMessage[SETUP_MESSAGE];
	snprintf(setupMessage, SETUP_MESSAGE, "%d\n%s\n%d\n%s\n",
			 nonce, name, firstClientPort, firstClientName);
	printf("Setup message: %d\n%s\n%d\n%s\n",
			 nonce, name, firstClientPort, firstClientName);
	Write(connectedSocket, setupMessage, strlen(setupMessage));
}

int handleNewConnection(int listeningSocket, FILE *log) {
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));//clear the struct
	socklen_t len = sizeof(address);
	int connectedSocket = Accept(listeningSocket, (SA *) &address, &len);
	
	fprintf(log, "client port: %d\n", ntohs(address.sin_port));
//	printf("client tcp port: %d\n", ntohs(address.sin_port));

	return connectedSocket;
}

int setupClient(int listeningSocket, FILE *log, char name[MAX_NAME], char firstClientName[MAX_NAME], const int count, uint32_t nonce, int *firstUdpPort){
	fd_set readingSet, writingSet;
	int maxFd = listeningSocket;
	FD_ZERO(&readingSet);
	FD_SET(listeningSocket, &readingSet);
	FD_ZERO(&writingSet);
	
	bool isReady = false;
	int connectedSocket = -1;
	while (!isReady){
		fd_set tempRead = readingSet;
		fd_set tempWrite = writingSet;
		int nReady;
		do {
			nReady = select(maxFd+1, &tempRead, &tempWrite, NULL, NULL);
		} while ( (nReady < 0) && (errno == EINTR));
		
		if (FD_ISSET(listeningSocket, &tempRead)){
			connectedSocket = handleNewConnection(listeningSocket, log);
			FD_SET(connectedSocket, &writingSet);
			if (connectedSocket > maxFd) {
				maxFd = connectedSocket;
			}
			
			//iterative: do not accept new client until the current client is ready
			FD_CLR(listeningSocket, &readingSet);
			
			if (--nReady <= 0) continue;
		}
		
		
		
		
		int socket = connectedSocket;
		if (socket < 0) continue;
		
		if (FD_ISSET(socket, &tempWrite)){
			sendSetupMessage(firstClientName, *firstUdpPort, name, nonce, socket);
			FD_CLR(socket, &writingSet);
			FD_SET(socket, &readingSet);
			if (--nReady <= 0) continue;
		}
		else if (FD_ISSET(socket, &tempRead)){
			uint16_t port = readClientUpdPort(socket, log);
			if (count == FIRST_CLIENT){
				*firstUdpPort = port;
			}
			FD_CLR(socket, &readingSet);
			
			
			isReady = true;
		}
	}
	
	printf("End of setupClient %s\n", name);
	return connectedSocket;
}

FILE * openManagerLogFile(int stage) {
    char fileName[MAX_FILE_NAME];
	snprintf(fileName, MAX_FILE_NAME, "stage%d.manager.out", stage);
	FILE *log = openOutputFile(fileName);
    return log;
}

int getFirstClient(int clients[]){
	for(int i = 0; i < MAX_CLIENT; i++){
		if (clients[i] >= 0){
			return clients[i];
		}
	}
	return -1;
}

void sendCommandToClient(int* clientSockets, char *command, char *name)
{
    int socket = getFirstClient(clientSockets);
    if (socket < 0){
        printError("Cannot find a client\n");
    }
    sendCommandAndWait(command, name, socket);
}

int findSocketWithName(int* clientSockets, char names[][MAX_NAME], char *name){
	for(int i = 0; i < MAX_CLIENT; i++){
		if (clientSockets < 0) continue;
		if (strncmp(name, names[i], MAX_NAME) == 0){
			return i;
		}
	}
	return -1;
}

void waitForRebuild(void) {
	int remain = sleep(MANAGER_SLEEP);
	while (remain > 0) {
		printf("Sleep interrupted %d\n", remain);
		remain = sleep(remain);
	}
}

void terminateClient(int* clientSockets, char names[][MAX_NAME], Command *command){
    int i = findSocketWithName(clientSockets, names, command->name);
	if (i < 0){
		fprintf(stderr, "Cannot find socket for client with name %s\n", command->name);
		return;
    }
	int socket = clientSockets[i];
    if (socket < 0){
		fprintf(stderr, "Cannot find socket for client with name %s\n", command->name);
		return;
    }
	
    sendCommandAndWait(command->type == END ? END_COMMAND : KILL_COMMAND, command->name, socket);
	if (command->type == KILL){
		waitForRebuild();
	}
	
	clientSockets[i] = -1;
}
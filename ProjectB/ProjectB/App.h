//
//  App.h
//  ProjectB
//
//  Created by Cuong Dong on 11/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_App_h
#define ProjectB_App_h

#include "Node.h"

typedef enum {NOT_DONE, ABSENT, PRESENT} SearchStatus;

typedef struct {
	int tcpSocket;
	bool isBusy;
	bool isSendOk;
	bool timeToDie;
	
	Node *node;
	
	//stage 7 store
	int count;
	
	//stage 7 search
	HashId searchNode[NUMBER_OF_SEARCH];
	SearchStatus searchDone[NUMBER_OF_SEARCH];
	char search[NUMBER_OF_SEARCH][MESSAGE_LENGTH];
} App;

void initApp(App *app, int tcp, Node *node);
void sendReadyToManager(App *app);
void sendOk(App *app);
void searchDone(App *app, Node *thisNode, char *name, int i);

void handleTcpCommand(App *app);
#endif

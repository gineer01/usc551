//
//  Config.c
//  ProjectA
//
//  Created by Cuong Dong on 9/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include "Util.h"
#include "Config.h"


bool isComment(char *line){
	return line[0] == '#';
}

int getStage(char *line){
	int n = -1;
	sscanf(line, "stage %d", &n);
	return n;
}

Config readConfigFile(FILE *input){
	const int BUFFER_SIZE = 1024;
	char line[BUFFER_SIZE];
	Config config = {-1, -1};
	
	bool isStage1 = false;
	bool num = false;
	bool nonce = false;
	
	while (fgets(line, BUFFER_SIZE, input) != NULL){
		if (isComment(line)){
			continue;
		}
		
		if (!isStage1){
			if (getStage(line) > 0){//not comment and stage line
				isStage1 = true;
				continue;
			}
			else {
				printError("Invalid line");
			}
		}
		
		int result = sscanf(line, "num_nodes %d", &config.c);
		if (result == 1) {
			num = true;
		}
		
		result = sscanf(line, "nonce %d", &config.nonce);
		if (result == 1) {
			nonce = true;
		}
	}
	
	if (isStage1 && num && nonce){	
		return config;
	}
	else {
		printError("Unable to parse all 'stage 1', 'num_nodes', and 'nonce'");
		return config;
	}
}

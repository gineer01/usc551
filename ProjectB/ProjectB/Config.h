//
//  Config.h
//  ProjectB
//
//  Created by Cuong Dong on 10/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef ProjectB_Config_h
#define ProjectB_Config_h

#include "stdint.h"
#include "stdbool.h"

typedef struct {
	int stage; //stage
	uint32_t nonce; // nonce
} Config;

Config readConfigFile(FILE *input);

typedef enum {START, TKCC, STORE, SEARCH, END, KILL} CommandType;

typedef struct {
	CommandType type;
	char *name;
} Command;

bool getNextCommand(FILE *input, Command *command);

#endif

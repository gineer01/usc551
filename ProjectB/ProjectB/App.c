//
//  App.c
//  ProjectB
//
//  Created by Cuong Dong on 11/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include "App.h"
#include "Unp.h"
#include "Util.h"

HashId masks[] = {0, 0x40000000, 0x80000000, 0xc0000000};
HashId getAltHash(HashId original, int i){
	return (original & 0x3FFFFFFF) | masks[i];
}


void initApp(App *app, int tcp, Node *node){
	app->tcpSocket = tcp;
	app->node = node;
	app->isBusy = false;
	app->isSendOk = false;
	app->timeToDie = false;
}

void sendReadyToManager(App *app){
	int nonce = app->node->nonce;
	int udpPort = app->node->node.port;
	int socket = app->tcpSocket;
	//compute modified nonce and send to manager
	pid_t id = getpid();
	int x = id + nonce;
	char clientInfo[256];
	snprintf(clientInfo, 256, "%d\n%d\n", x, udpPort);
	Write(socket, clientInfo, strlen(clientInfo));
}

void sendOk(App *app){
	char *message = "ok\n";
	Write(app->tcpSocket, message, strlen(message));
	app->isSendOk = false;
}

void doStore(Node *thisNode, App *app, HashId hash, char *name){
	printf("doStore : %08x %s\n", hash, name);
	
	HashId nodeId = thisNode->node.hash;
	HashId successorId = thisNode->fingers[SUCCESSOR_INDEX].hash;
	HashId predecessorId = thisNode->fingers[PREDECESSOR_INDEX].hash;
	
	if ((nodeId == successorId) || isInRangeLeftOpen(hash, predecessorId, nodeId)){
		printf("doStore : do it locally. %08x %08x %08x\n", hash, predecessorId, nodeId);
		storeData(thisNode, hash, name);
		if (thisNode->stage == 7){
			app->count++;
			app->isSendOk = app->count == NUMBER_OF_COPIES;
		}
		else {
			app->isSendOk = true;
		}
		fprintf(thisNode->k->log, "add %s with hash 0x%08x to node 0x%08x\n", name, hash, nodeId);
		fprintf(stdout, "add %s with hash 0x%08x to node 0x%08x\n", name, hash, nodeId);
	}
	else if (isInRangeLeftOpen(hash, nodeId, successorId)){
		printf("doStore : do it at successor. %08x %08x %08x\n", hash, predecessorId, nodeId);
		NodeInfo nodeToAsk = thisNode->fingers[SUCCESSOR_INDEX];
		Message *newQuery = malloc(sizeof(Message));
		
		newQuery->type = STORE_PLEASE_Q;
		newQuery->receiverId = nodeToAsk.hash;
		newQuery->queryId = hash;
		newQuery->length = (unsigned int) strlen(name);
		memcpy(newQuery->s, name, newQuery->length);
		
		ExecContext *context = malloc(sizeof(ExecContext));
		initContext(context, nodeToAsk.hash, STORE_PLEASE_R, APP_STORE, 2, NULL);
		context->local.app = app;
		context->local.storeId = hash;
		
		queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, context);
	}
	else {
		//send query Stores to closestPrecedingFinger;
		NodeInfo nodeToAsk = closestPrecedingFinger(thisNode, hash);
		printf("doStore : query. %08x with %08x port %d\n", hash, nodeToAsk.hash, nodeToAsk.port);
		
		Message *newQuery = malloc(sizeof(Message));
		
		newQuery->type = STORES_Q;
		newQuery->receiverId = nodeToAsk.hash;
		newQuery->queryId = hash;
		
		ExecContext *context = malloc(sizeof(ExecContext));
		initContext(context, nodeToAsk.hash, STORES_R, APP_STORE, 1, name);
		context->local.app = app;
		context->local.queryId = hash;
		
		context->local.storeId = hash;
		
		queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, context);
	}
}



void searchDone(App *app, Node *thisNode, char *name, int i) {
    if (thisNode->stage == 7){
		int theOtherSearch = i == 0 ? 1 : 0;
		
		if (app->searchDone[theOtherSearch] != NOT_DONE){
			if ((app->searchDone[i] == PRESENT) && (app->searchDone[theOtherSearch] == PRESENT)){
				bool agree = strcmp(app->search[i], app->search[theOtherSearch]) == 0;
				fprintf(thisNode->k->log, "search %s to node 0x%08x and 0x%08x, key PRESENT and %s\n", name, app->searchNode[i], app->searchNode[theOtherSearch],
						agree ? "VERIFIED" : "DISAGREE");
				fprintf(stdout, "search %s to node 0x%08x and 0x%08x, key PRESENT and %s\n", name, app->searchNode[i], app->searchNode[theOtherSearch],
						agree ? "VERIFIED" : "DISAGREE");
			}
			else {
				fprintf(stderr,  "search %s to node 0x%08x and 0x%08x, one reports PRESENT and one reports ABSENT\n", name, app->searchNode[i], app->searchNode[theOtherSearch]);
			}
			
			app->isSendOk = true;
		}
    } else {
        app->isSendOk = true;
    }
}

void doSearch(Node *thisNode, App *app, HashId hash, char *name, int ith){	
	HashId nodeId = thisNode->node.hash;
	HashId successorId = thisNode->fingers[SUCCESSOR_INDEX].hash;
	HashId predecessorId = thisNode->fingers[PREDECESSOR_INDEX].hash;
	
	printf("Node %08x doSearch : %08x %s\n", nodeId, hash, name);
	
	if ((nodeId == successorId) || isInRangeLeftOpen(hash, predecessorId, nodeId)){
		bool found = containsData(thisNode, hash);
		fprintf(thisNode->k->log, "search %s to node 0x%08x, key %s\n", name, nodeId,
				found ? "PRESENT" : "ABSENT");
		fprintf(stdout, "search %s to node 0x%08x, key %s\n", name, nodeId,
				found ? "PRESENT" : "ABSENT");
		
		app->searchDone[ith] = found ? PRESENT : ABSENT;
		app->searchNode[ith] = nodeId;
		if (found) {
			for (int i = 0; i < MAX_DATA; i++) {
				if (thisNode->data[i] == hash){
					strcpy(app->search[ith], thisNode->dataS[i]);
				}
			}
		}
		
		searchDone(app, thisNode, name, ith);
	}
	else if (isInRangeLeftOpen(hash, nodeId, successorId)){
		NodeInfo nodeToAsk = thisNode->fingers[SUCCESSOR_INDEX];
		Message *newQuery = malloc(sizeof(Message));
		
		newQuery->type = STORES_Q;
		newQuery->receiverId = nodeToAsk.hash;
		newQuery->queryId = hash;
		
		ExecContext *context = malloc(sizeof(ExecContext));
		initContext(context, nodeToAsk.hash, STORES_R, APP_SEARCH, 2, name);
		context->local.app = app;
		context->local.queryId = hash;
		context->local.searchAttempt = ith;
		
		queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, context);
	}
	else {
		//send query Stores to closestPrecedingFinger;
		NodeInfo nodeToAsk = closestPrecedingFinger(thisNode, hash);
		Message *newQuery = malloc(sizeof(Message));
		
		newQuery->type = STORES_Q;
		newQuery->receiverId = nodeToAsk.hash;
		newQuery->queryId = hash;
		
		ExecContext *context = malloc(sizeof(ExecContext));
		initContext(context, nodeToAsk.hash, STORES_R, APP_SEARCH, 1, name);
		context->local.app = app;
		context->local.queryId = hash;
		context->local.searchAttempt = ith;
		
		queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, context);
	}
}

void doEnd(Node *thisNode, App *app){
	app->isBusy = true;//not listening for more command until sending OK
	
	
	NodeInfo nodeToAsk = thisNode->fingers[SUCCESSOR_INDEX];
	Message *newQuery = malloc(sizeof(Message));
	
	newQuery->type = LEAVING_Q;
	newQuery->receiverId = nodeToAsk.hash;
	newQuery->queryId = thisNode->node.hash;
	
	queueMessageAndContext(thisNode->k, newQuery, nodeToAsk.port, NULL);
}


void executeStore(char* name, App *app, Node *thisNode) {	
	app->isBusy = true;//not listening for more command until sending OK
	app->count = 0;
	
	HashId hash = triadHash(thisNode->nonce, name);
    if (app->node->stage != 7) {
        doStore(thisNode, app, hash, name);
		return;
    }
    else {
		for (int i = 0; i < NUMBER_OF_COPIES; i++) {			
			doStore(thisNode, app, getAltHash(hash, i), name);
		}
    }
}

void executeSearch(char* name, Node *thisNode, App *app) {
    HashId hash = triadHash(thisNode->nonce, name);
	
	app->isBusy = true;//not listening for more command until sending OK
	for (int i = 0; i < NUMBER_OF_SEARCH; i++){
		app->searchDone[i] = NOT_DONE;
	}
	
    if (thisNode->stage == 7){
		for (int i = 0; i < NUMBER_OF_COPIES; i++) {
			HashId left = getAltHash(hash, i);
			HashId right = getAltHash(hash, (i + 1) % NUMBER_OF_COPIES);
			
			if (isInRangeLeftOpen(thisNode->node.hash, left, right)){
				doSearch(thisNode, app, right, name, 0);
				HashId nextRight = getAltHash(hash, (i + 2) % NUMBER_OF_COPIES);
				doSearch(thisNode, app, nextRight, name, 1);
			}
		}
	}
	else {
		doSearch(thisNode, app, hash, name, 1);
	}
}

void executeEnd(char* name, Node *thisNode, App *app) {
    HashId hash = triadHash(thisNode->nonce, name);
    if (hash != thisNode->node.hash){
        fprintf(stderr, "WARNING: mismatch hash\n");
    }
    doEnd(thisNode, app);
}

void handleTcpCommand(App *app){
	Node *thisNode = app->node;
	printf("TCP command received\n");
	
	char buffer[BUFFER_SIZE];
	int total = readSocket(app->tcpSocket, buffer, BUFFER_SIZE, 2);
	if (total <= 0) {
		app->timeToDie = true;
		return;
	}
	
	buffer[total] = 0;
	printf("TCP command (size %d) : %s\n", total, buffer);
	
	char command[20];
	char name[MAX_NAME];
	sscanf(buffer, " %s %s", command, name);
	printf("Command : %s. Name: %s\n", command, name);
		
	if (strcmp(command, STORE_COMMAND) == 0) {
		executeStore(name, app, thisNode);
	} else if (strcmp(command, SEARCH_COMMAND) == 0) {
		executeSearch(name, thisNode, app);
	} else if (strcmp(command, END_COMMAND) == 0) {
		executeEnd(name, thisNode, app);
	} else if (strcmp(command, KILL_COMMAND) == 0) {
		HashId hash = triadHash(thisNode->nonce, name);
		if (hash != thisNode->node.hash){
			fprintf(stderr, "WARNING: mismatch hash\n");
		}
		app->timeToDie = true;
	}
	else {
		fprintf(stderr, "Unexpected command %s\n", command);
	}
}